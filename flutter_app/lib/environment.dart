class Environment {
  static String IP = "";
  static String PORT = "";



  static String getViewDevice() {
    return "http://" + Environment.IP + ":${Environment.PORT}/view_device/";
  }

  static String getAuthLogin() {
    return "http://" + Environment.IP + ":${Environment.PORT}/auth";
  }

  static String getPostDevice() {
    return "http://" + Environment.IP + ":${Environment.PORT}/add_device/";
  }

  static String getDeleteDeive() {
    return "http://" + Environment.IP + ":${Environment.PORT}/delete_device/";
  }

  static String getUrlSignUp() {
    return "http://" + Environment.IP + ":${Environment.PORT}/signup";
  }

  static String getVerify() {
    return "http://" + Environment.IP + ":${Environment.PORT}/verify";
  }

  static String getChart() {
    return "http://" + Environment.IP + ":${Environment.PORT}/json/";
  }

  static String getViewInfo() {
    return "http://" + Environment.IP + ":${Environment.PORT}/view_info/";
  }

  static String getChangePass() {
    return "http://" + Environment.IP + ":${Environment.PORT}/change_pass/";
  }

  static String getPostEditDevice() {
    return "http://" +  Environment.IP + ":${Environment.PORT}/edit_device/";
  }

  static String getGetDays() {
    return "http://" + Environment.IP + ":${Environment.PORT}/get_days/";
  }


}