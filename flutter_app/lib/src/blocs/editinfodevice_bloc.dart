// @dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/environment.dart';
import 'package:flutter_app/src/blocs/url.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

class EditInfoDeviceBloc {
  final _imeiController = new BehaviorSubject<String>();
  final _nameController = new BehaviorSubject<String>();
  final _directionController = new BehaviorSubject<String>();
  final _numberOfPinController = new BehaviorSubject<String>();
  final _DeviceKeyController = new BehaviorSubject<String>();
  final _SoftwareKeyController = new BehaviorSubject<String>();
  final _VoutController = new BehaviorSubject<String>();
  final _VoutMaxController = new BehaviorSubject<String>();
  final _tenThietBiController = new BehaviorSubject<String>();

  Stream<String> get imeiStream => _imeiController.stream;
  Stream<String> get nameControllerStream => _nameController.stream;
  Stream<String> get directionStream => _directionController.stream;
  Stream<String> get numberOfPinStream => _numberOfPinController.stream;
  Stream<String> get DeviceKeyStream => _DeviceKeyController.stream;
  Stream<String> get SoftwareKeyStream => _SoftwareKeyController.stream;
  Stream<String> get VoutStream => _VoutController.stream;
  Stream<String> get VoutMaxStream => _VoutMaxController.stream;
  Stream<String> get tenThietBiStream => _tenThietBiController.stream;
  String newDevice;

  bool isValidInfo(
      String nameDevice,
      String directiondevice,
      String numbersPinDevice,
      String VoutDevice,
      String VoutMaxDevice,
      String tenThietBi,
      String DeviceKey,
      String SoftwareKey) {
    bool isValid = true;
    if (nameDevice.isEmpty) {
      _nameController.add("Nhập mã sản phẩm");
      isValid = false;
    } else {
      _nameController.add(null);
    }
    if (directiondevice.isEmpty) {
      _directionController.add("Nhập hướng góc");
      isValid = false;
    } else {
      _directionController.add(null);
    }
    if (numbersPinDevice.isEmpty) {
      _numberOfPinController.add("Nhập số lượng tấm pin");
      isValid = false;
    } else {
      _numberOfPinController.add(null);
    }
    if (VoutDevice.isEmpty) {
      _VoutController.add("Nhập điện áp đầu ra");
      isValid = false;
    } else {
      _VoutController.add(null);
    }
    if (VoutMaxDevice.isEmpty) {
      _VoutMaxController.add("Nhập điện áp tối đa đầu vào");
      isValid = false;
    } else {
      _VoutMaxController.add(null);
    }

    if (tenThietBi.isEmpty) {
      _tenThietBiController.add("Nhập tên thiết bị");
      isValid = false;
    } else {
      _tenThietBiController.add(null);
    }
    if (DeviceKey.isEmpty) {
      _DeviceKeyController.add("Nhập devicekey");
      isValid = false;
    } else {
      _DeviceKeyController.add(null);
    }
    if (SoftwareKey.isEmpty) {
      _SoftwareKeyController.add("Nhập software key");
      isValid = false;
    } else {
      _SoftwareKeyController.add(null);
    }

    return isValid;
  }

  bool isValidInfo2(
      String tenThietBi) {
    bool isValid = true;
    if (tenThietBi.isEmpty) {
      _tenThietBiController.add("Nhập tên thiết bị");
      isValid = false;
    } else {
      _tenThietBiController.add(null);
    }

    return isValid;
  }

  Future<String> editInfoDevice(String username,String imei, String nameDevice,
      String directiondevice,
      String numbersPinDevice,
      String VoutDevice,
      String VoutMaxDevice,
      String tenThietBi,
      String DeviceKey,
      String SoftwareKey) async {
    if (!isValidInfo(nameDevice,
         directiondevice,
         numbersPinDevice,
         VoutDevice,
         VoutMaxDevice,
         tenThietBi,
         DeviceKey,
         SoftwareKey)) {
      return "Thông tin không hợp lệ";
    }
    //goi post
    newDevice =
        await posteditDevice(username,imei, nameDevice,
            directiondevice,
            numbersPinDevice,
            VoutDevice,
            VoutMaxDevice,
            tenThietBi,
            DeviceKey,
            SoftwareKey);
    if (newDevice == null) {
      return ("Cập nhật thông tin không thành công");
    }
    return null;
  }

  Future<String> posteditDevice(String username,String imei, String nameDevice,
      String directiondevice,
      String numbersPinDevice,
      String VoutDevice,
      String VoutMaxDevice,
      String tenThietBi,
      String DeviceKey,
      String SoftwareKey) async {

    final String apiUrl = Environment.getPostEditDevice() + imei + "/"+username;
    final response = await http.post(apiUrl, body: {
      "nameD": nameDevice,
      "directionD": directiondevice,
      "numbersPinD": numbersPinDevice,
      "tenThietBi": tenThietBi,
      "vD": VoutDevice,
      "vmaxD": VoutMaxDevice,
      "devicekeyD": DeviceKey,
      "softwarekeyD": SoftwareKey,
    });

    if (response.statusCode == 200) {
      return imei;
    } else {
      return null;
    }
  }

  Future<String> editInfoDevice2(String username,String imei, String tenThietBi,
       ) async {
    if (!isValidInfo2(tenThietBi)) {
      return "Thông tin không hợp lệ";
    }
    //goi post
    newDevice = await posteditDevice2(username,imei, tenThietBi);
    if (newDevice == null) {
      return ("Cập nhật thông tin không thành công");
    }
    return null;
  }

  Future<String> posteditDevice2(String username,String imei,
      String tenThietBi,) async {

    final String apiUrl = Environment.getPostEditDevice() + imei + "/"+username;
    final response = await http.post(apiUrl, body: {
      "tenThietBi": tenThietBi,
    });

    if (response.statusCode == 200) {
      return imei;
    } else {
      return null;
    }
  }




  void dispose() {
    _nameController.close();
    _numberOfPinController.close();
    _directionController.close();
    _numberOfPinController.close();
    _tenThietBiController.close();
    _DeviceKeyController.close();
    _SoftwareKeyController.close();
  }
}
