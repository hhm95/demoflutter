// @dart=2.9
import 'dart:async';
import 'package:flutter_app/environment.dart';
import 'package:flutter_app/src/blocs/url.dart';
import 'package:flutter_app/src/others/EasyJsonParse/UserViewDevice.dart';
import 'package:flutter_app/src/others/EasyJsonParse/Users.dart';
import 'package:flutter_app/src/validators/validations.dart';
import 'package:rxdart/rxdart.dart';

import '../validators/validations.dart';
import 'package:http/http.dart' as http;

class LoginBloc {
  final _userController = new BehaviorSubject<String>();
  final _passController = new BehaviorSubject<String>();

  final _linkController = new BehaviorSubject<String>();

  final _loginController = new BehaviorSubject<String>();

  Stream<String> get userStream => _userController.stream;

  Stream<String> get passStream => _passController.stream;

  Stream<String> get linkStream => _linkController.stream;

  Stream<String> get loginStream => _loginController.stream;

  User user;

  bool isValidInfo(String username, String password, String link) {
    bool isValid = true;
    if (username.isEmpty) {

      _userController.add('Nhập tên tài khoản');
      isValid = false;
    } else {
      if (!Validations.isValidUser(username)) {
        _userController.add("Tên tài khoản không hợp lệ");
        isValid = false;
      } else {
        _userController.add(null);
      }
    }
    if (password.isEmpty) {
      _passController.add('Nhập mật khẩu');
      isValid = false;
    } else {
      if (!Validations.isValidPass(password)) {
        _passController.add("Mật khẩu không hợp lệ");
        isValid = false;
      } else {
        _passController.add(null);
      }
    }
    if (link.isEmpty) {
      _linkController.add('Nhập liên kết');
      isValid = false;
    } else {
      if (!Validations.isValidLink(link)) {
        _linkController.add("Liên kết không hợp lệ");
        isValid = false;
      } else {
        _linkController.add(null);
      }
    }
    return isValid;
  }

  Future<String> login(String username, String password, String link) async {
    if (!isValidInfo(username, password, link)) {
      return "Thông tin không hợp lệ";
    }
    //goi post
    user = await authLogin(username, password, link);
    if (user == null) {
      return ("Sai thông tin tài khoản hoặc mật khẩu");
    }
    return null;
  }

  Future<User> authLogin(String username, String password, String link) async {
    //final String apiUrl = "http://192.168.1.106:3000/auth";
    final String apiUrl = Environment.getAuthLogin();

    final response = await http.post(apiUrl, body: {
      "username": username,
      "password": password,
    });
    if (response.statusCode == 200) {
      final String responseString = response.body;
      return userFromJson(responseString);
    } else {
      return null;
    }
  }

  void dispose() {
    _userController.close();
    _passController.close();
  }
}
