// @dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/environment.dart';
import 'package:flutter_app/src/blocs/url.dart';
import 'package:flutter_app/src/validators/validations.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

class ChangePassBloc {
  final _oldPassController = new BehaviorSubject<String>();
  final _newPassController = new BehaviorSubject<String>();
  final _newPass2Controller = new BehaviorSubject<String>();

  Stream<String> get oldPassStream => _oldPassController.stream;
  Stream<String> get newPassStream => _newPassController.stream;
  Stream<String> get newPass2Stream => _newPass2Controller.stream;

  String changePass;

  bool isValidInfo(String oldPass, String newPass, String newPass2){
    bool isValid = true;

    if(oldPass.isEmpty){
      _oldPassController.add("Nhập mật khẩu cũ");
      isValid = false;
    }else{
      if(!Validations.isValidPass(oldPass)){
        _oldPassController.add("Mật khẩu cũ không hợp lệ");
        isValid = false;
      }else{
        _oldPassController.add(null);
      }
    }
    if(newPass.isEmpty){
      _newPassController.add("Nhập mật khẩu mới");
      isValid = false;
    }else{
      if(!Validations.isValidPass(newPass)){
        _newPassController.add("Mật khẩu mới không hợp lệ");
      isValid = false;
    }else{
        _newPassController.add(null);
    }
    }

    if(newPass2.isEmpty){
      _newPass2Controller.add("Nhập lại mật khẩu");
      isValid = false;
    }else{
      if(!Validations.isValidPass(newPass2) || (newPass2 != newPass)){
        _newPass2Controller.add("Kiểm tra lại");
        isValid = false;
      }else{
        _newPass2Controller.add(null);
      }
    }

    return isValid;
  }

  Future<String> changePassword(String username, String oldPass, String newPass, String newPass2) async{
    if(!isValidInfo(oldPass, newPass, newPass2)){
      return "Thông tin không hợp lệ";
    }
    //goi post
    changePass = await onchangePassword(username, oldPass, newPass);
    if(changePass == null){
      return ("Có lỗi");
    }
    return null;
  }

  Future<String> onchangePassword(String username, String oldPass, String newPass) async{

    final String apiUrl = Environment.getChangePass()+username;
    final response = await http.post(apiUrl,body:{
      "username"  : username,
      "oldPass"   : oldPass,
      "newPass"   : newPass,
    });

    if(response.statusCode == 200){
      EasyLoading.showToast("Thay đổi mật khẩu thành công");
      return username;
    }
    else{
      return null;
    }
  }

  void dispose(){
    _oldPassController.close();
    _newPassController.close();
    _newPass2Controller.close();
  }


}
