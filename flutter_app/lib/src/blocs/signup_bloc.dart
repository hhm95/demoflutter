// @dart=2.9
import 'dart:async';
import 'package:flutter_app/environment.dart';
import 'package:flutter_app/src/blocs/url.dart';
import 'package:flutter_app/src/others/EasyJsonParse/UserViewDevice.dart';
import 'package:flutter_app/src/others/EasyJsonParse/Users.dart';
import 'package:flutter_app/src/validators/validations.dart';
import 'package:rxdart/rxdart.dart';

import '../validators/validations.dart';
import 'package:http/http.dart' as http;

class SignUpBloc {

  final _userController = new BehaviorSubject<String>();
  final _passController = new BehaviorSubject<String>();
  final _phoneController = new BehaviorSubject<String>();
  final _signupController = new BehaviorSubject<String>();
  final _linkController =  new BehaviorSubject<String>();

  Stream<String> get userStream  => _userController.stream;
  Stream<String> get passStream  => _passController.stream;
  Stream<String> get phoneStream => _phoneController.stream;
  Stream<String> get loginStream => _signupController.stream;
  Stream<String> get linkStream => _linkController.stream;

  String create_user;

  bool isValidInfo(String username, String password, String phone, String link){
    bool isValid = true;
    if (username.isEmpty) {
      _userController.add('Nhập tài khoản');
      isValid = false;
    } else {
      if(!Validations.isValidUser(username)){
        _userController.add("Tài khoản không hợp lệ");
        isValid = false;
      }else{
        _userController.add(null);
      }
    }
    if (password.isEmpty) {
      _passController.add('Nhập mật khẩu');
      isValid = false;
    }else{
      if(!Validations.isValidPass(password)){
        _passController.add("Mật khẩu không hợp lệ");
        isValid = false;
      }else{
        _passController.add(null);
      }
    }
    if(phone.isEmpty){
      _phoneController.add("Nhập số điện thoại");
      isValid = false;
    }else{
      if(!Validations.isValidPhone(phone)){
        _phoneController.add("Số điện thoại dài 10 kí tự");
        isValid = false;
      }else{
        _phoneController.add(null);
      }
    }

    if(link.isEmpty){
      _linkController.add("Nhập liên kết");
      isValid = false;
    }else{
      if(!Validations.isValidLink(link)){
        _linkController.add("Liên kết không hợp lệ");
        isValid = false;
      }else{
        _linkController.add(null);
      }
    }

    return isValid;
  }

  Future<String> signup(String username, String password, String phone, String link) async {
    if (!isValidInfo(username, password, phone, link)) {
      return "Thông tin không hợp lệ";
    }
    //goi post
    final String apiUrl = Environment.getUrlSignUp();
    final response = await http.post(apiUrl, body: {
      "email": username,
      "password": password,
      "phonenumber": "84"+phone.substring(1),//convert sdt 84+(0)38222...
    });
    if (response.statusCode == 200) {
      return 'Nhập mã OTP nhận được';
    }
    return "Kiểm tra lại thông tin";
  }

  Future<String> verify(String otpcode) async{
    final String apiUrl = Environment.getVerify();
    final response = await http.post(apiUrl, body: {
      "codeverify": otpcode,
    });
    if (response.statusCode == 200) {
      // final String responseString = response.body;
      return null;
    } else {
      return "Sai mã OTP";
    }

  }


  void dispose(){
    _userController.close();
    _passController.close();
  }
}