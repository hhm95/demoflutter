// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_app/environment.dart';
import 'package:flutter_app/src/blocs/url.dart';
import 'package:flutter_app/src/others/EasyJsonParse/GetDays.dart';
import 'package:flutter_app/src/others/EasyJsonParse/ViewChart.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

class ViewChartBloc{
  String imei;
  List<DataChart> dataChart;
  GetSmonth dataSmonth;

  final streamController1 = BehaviorSubject<List<DataChart>>();
  Stream<List<DataChart>> get streamDevices1 => streamController1.stream;

  final streamController2 = BehaviorSubject<GetSmonth>();
  Stream<GetSmonth> get streamDevices2 => streamController2.stream;

  Future<String> viewChart(String imei) async{
    dataChart= await getdataChart(imei);
    streamController1.add(dataChart);
    if(dataChart==null){
      return "Get Chart null";
    }
    return null;
  }
  Future<List<DataChart>> getdataChart(String imei) async{
    final String apiUrl = Environment.getChart()+imei;
    final response = await http.get(apiUrl);
    if(response.statusCode == 200){
      final String responseString = response.body;
      return dataChartFromJson(responseString);
    }else{
      return null;
    }
  }
  Future<String> getDays(String imei, String mmyy) async{
    dataSmonth= await onGetDays(imei,mmyy);
    streamController2.add(dataSmonth);
    if(dataSmonth==null){
      return "Get Days null";
    }
    return null;
  }
  Future<GetSmonth> onGetDays(String imei, String mmyy) async{

    final String apiUrl = Environment.getGetDays()+imei+"/"+mmyy;
    final response = await http.get(apiUrl);
    if(response.statusCode == 200){
      final String responseString = response.body;
      return getSmonthFromJson(responseString);
    }else{
      //EasyLoading.showToast("get null");
      return null;
    }
  }
  dispose() {
    streamController1.close();
    streamController2.close();
  }

}