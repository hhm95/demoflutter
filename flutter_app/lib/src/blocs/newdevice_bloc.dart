// @dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/environment.dart';
import 'package:flutter_app/src/blocs/url.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

class AddNewDeviceBloc {
  final _imeiController = new BehaviorSubject<String>();
  final _deviceKeyController = new BehaviorSubject<String>();
  final _deviceNameController = new BehaviorSubject<String>();

  Stream<String> get imeiStream => _imeiController.stream;
  Stream<String> get deviceKeyStream => _deviceKeyController.stream;
  Stream<String> get deviceNameStream => _deviceNameController.stream;

  String newDevice;

  bool isValidInfo(String imei, String deviceKey, String deviceName){
    bool isValid = true;
    if(imei.isEmpty){
      _imeiController.add("Nhập imei");
      isValid = false;
    }else{
      _imeiController.add(null);
    }
    if(deviceKey.isEmpty){
      _deviceKeyController.add("Nhập DeviceKey");
      isValid = false;
    }else{
      _deviceKeyController.add(null);
    }

    if(deviceName.isEmpty){
      _deviceNameController.add("Nhập DeviceName");
      isValid = false;
    }else{
      _deviceNameController.add(null);
    }

    return isValid;
  }

  Future<String> addNewDevice(String username, String imei, String deviceKey, String deviceName) async{
    if(!isValidInfo(imei, deviceKey, deviceName)){
      return "Thông tin không hợp lệ";
    }
    //goi post
    newDevice = await addDevice(username, imei, deviceKey, deviceName);
    if(newDevice == null){
      return ("Thiết bị đã tồn tài hoặc sai thông tin");
    }
    return null;
  }

  Future<String> addDevice(String username, String imei, String deviceKey, deviceName) async{
    final String apiUrl = Environment.getPostDevice()+username;
    final response = await http.post(apiUrl,body:{
      "imei"         : imei,
      "device_key"   : deviceKey,
      "device_name"  : deviceName
    });

    if(response.statusCode == 200){
      return username;
    }
    else{
      return null;
    }
  }

  void dispose(){
    _imeiController.close();
    _deviceKeyController.close();
    _deviceNameController.close();
  }


}
