// @dart=2.9
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/environment.dart';
import 'package:flutter_app/src/blocs/url.dart';
import 'package:flutter_app/src/others/EasyJsonParse/GetLocation.dart';
import 'package:flutter_app/src/others/EasyJsonParse/ViewInfo.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

class GetLocationBloc {
  String imei;
  GetLocation viewInfo;


  final infoController = new BehaviorSubject<GetLocation>();
  Stream<GetLocation> get streamInfo => infoController.stream;


  Future<dynamic> getLocbyM(String mcc, String mnc, String lac, String cid) async {
    viewInfo = await getLocation(mcc,mnc,lac,cid);
    if(!infoController.isClosed){
      infoController.add(viewInfo);
    }
    if (viewInfo == null) {
      return "null";
    }
    return null;
  }

  Future<GetLocation> getLocation(String mcc, String mnc, String lac, String cid) async {
    final String apiUrl = "https://us1.unwiredlabs.com/v2/process.php";//string

    final Map<String, dynamic> activityData = {
      "token": "pk.bd633d2821bb6cde1c665d8f5aa3b3dc",
      "radio": "gsm",
      "mcc": 452,
      "mnc": 4,
      "cells": [{
        "lac": 41054,
        "cid": 15603
      }],
      "address": 1
    };

    // final response = await http.post(apiUrl, body: {
    //   "token": "pk.bd633d2821bb6cde1c665d8f5aa3b3dc",
    //   "radio": "gsm",
    //   "mcc": 452,
    //   "mnc": 4,
    //   "cells": [{
    //     "lac": 41054,
    //     "cid": 15603
    //   }],
    //   "address": 1
    // });

    final response = await http.post(apiUrl, body: json.encode(activityData),);

    if (response.statusCode == 200) {
      print('status 200');
      print(response.body);
      final String responseString = response.body;
      return getLocationFromJson(responseString);
    } else {
      print('status null');
      return null;
    }
  }

  void dispose() {
    infoController.close();
  }
}
