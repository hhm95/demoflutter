// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_app/environment.dart';
import 'package:flutter_app/src/blocs/url.dart';
import 'package:flutter_app/src/others/EasyJsonParse/UserViewDevice.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

class ViewDeviceBloc {
  String username;
  String user_delete;
  UserViewDevice userViewDevice;

  final streamController = BehaviorSubject<UserViewDevice>();
  Stream<UserViewDevice> get streamDevices => streamController.stream;

  Future<String> viewDevice(String username) async {
    //Can check username

    //goi get
    userViewDevice = await getViewDevice(username);
    //print("ddthanh ${userViewDevice.lists.length}");
    // if(userViewDevice.lists.length > 0) {
      userViewDevice.lists.forEach((element) {
        userViewDevice.devices.forEach((device) {
          if (device.imei == element.imei.substring(0, 12)) {
            element.name = device.deviceName;
          }
        });
      });
    // }
    if (!streamController.isClosed) {
      streamController.add(userViewDevice);
    }
    if (userViewDevice == null) {
      
      return ("Get ViewDevice null");
    }
    return null;
  }

  Future<UserViewDevice> getViewDevice(String username) async{
    //final String apiUrl = "http://192.168.1.106:3000/view_device/"+username;
    final String apiUrl = Environment.getViewDevice()+username;
    final response = await http.get(apiUrl);
    if (response.statusCode == 200) {
      final String responseString = response.body;
      return userViewDeviceFromJson(responseString);
    } else {
      EasyLoading.showToast("Mất kết nối");
      return null;
    }
  }

  Future<String> delete(String username, String imei) async{
    final String apiUrl = Environment.getDeleteDeive()+username+"/"+imei;
    final response = await http.get(apiUrl);

    if (response.statusCode == 200) {
      final String responseString = response.body;
      //gọi lại api o day
      userViewDevice = await getViewDevice(username);
      streamController.add(userViewDevice);
      return user_delete = username;
    } else {
      return "Lỗi xóa";
    }

  }
  dispose() {
    streamController.close();
  }
}