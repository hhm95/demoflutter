// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_app/environment.dart';
import 'package:flutter_app/src/blocs/url.dart';
import 'package:flutter_app/src/others/EasyJsonParse/ViewInfo.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

class ViewInfoBloc {
  String imei;
  List<ViewInfo> viewInfo;


  final infoController = new BehaviorSubject<List<ViewInfo>>();
  Stream<List<ViewInfo>> get streamInfo => infoController.stream;


  Future<String> showInfo(String imei) async {
    viewInfo = await getdataInfo(imei);
    if(!infoController.isClosed){
      infoController.add(viewInfo);
    }
    if (viewInfo == null) {
      return "null";
    }
    return null;
  }

  Future<List<ViewInfo>> getdataInfo(String imei) async {
    final String apiUrl = Environment.getViewInfo() + imei;
    final response = await http.get(apiUrl);
    if (response.statusCode == 200) {
      final String responseString = response.body;
      return viewInfoFromJson(responseString);
    } else {
      return null;
    }
  }

  void dispose() {
    infoController.close();
  }
}
