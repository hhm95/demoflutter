import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/app/home/home_screen.dart';
import 'package:flutter_app/src/common/app_colors.dart';
import 'package:flutter_app/src/common/app_text_style.dart';

class UserProfileScreen extends StatefulWidget {

  final Persion userName;
  UserProfileScreen({this.userName});  ///UserProfileScreen('manh') //bat buoc


  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody()
    );
  }

  _buildAppBar() {
    return AppBar(
      leading: IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.black),
        onPressed: () => Navigator.of(context).pop(),
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(widget.userName?.name.toString() ?? 'Profile', style: AppTextStyle.t16B.copyWith(color: AppColors.dark),),
          IconButton(
            icon: Icon(
              Icons.more_vert,
              color: Colors.white,
              size: 35,
            ),
            onPressed: () {

            },
          ),
        ],
      ),
    );
  }

  _buildBody(){
    return Padding(
      padding: const EdgeInsets.fromLTRB(18, 18, 18, 0),
      child: Column(
        children: [
          _buildHeaderWidget(),
          SizedBox(height: 10,),
          _buildFeaturedPhotos(),
          Expanded(child: _buidAllPhotos()),
        ],
      ),
    );
  }

  _buiBottomNavigation() {
    return BottomAppBar(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(icon: Icon(Icons.home), onPressed: () {}),
            IconButton(icon: Icon(Icons.mark_email_unread), onPressed: () {}),
            IconButton(icon: Icon(Icons.add_alert), onPressed: () {}),
            IconButton(icon: Icon(Icons.person), onPressed: () {}),
          ],
        ),
      ),
    );
  }

  _buildHeaderWidget() {
    return Container(
      // height: 260,
      child: Column(
        children: [
          Container(
            height: 60,
            child: Row(
              children: [
                InkWell(
                  onTap: () {},
                  child: Container(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Image.asset('assets/images/pic31.png'),
                    ),),
                ),
                SizedBox(width: 20,),
                Expanded(
                  flex: 1,
                  child: ButtonTheme(
                    child: FlatButton(
                      color: AppColors.blue,
                      onPressed: (){},
                      child: Row(
                        children:[
                          Icon(
                              Icons.check,
                              color: Colors.white,
                              size: 25,
                            ),

                          Expanded(
                            child: Text('Following', style: AppTextStyle.t12N.copyWith(color: Colors.white)
                            ),
                          ),],
                      ),
                      textColor: Color.alphaBlend(Colors.red, Colors.black),
                      shape: RoundedRectangleBorder(side: BorderSide(
                          color: Colors.blue,
                          width: 1,
                          style: BorderStyle.solid
                      ), borderRadius: BorderRadius.circular(10)),
                    ),
                  ),
                ),
                SizedBox(width: 16,),
                Expanded(
                  flex: 1,
                  child: FlatButton(
                    color: Colors.white,
                    onPressed: (){},
                    child: Row(
                      children:[
                        Icon(
                          Icons.message,
                          color: Colors.blue,
                          size: 25,
                        ),

                        Expanded(
                          child: Text('Message', style: AppTextStyle.t12N.copyWith(color: Colors.blue)
                          ),
                        ),],
                    ),
                    textColor: Color.alphaBlend(Colors.red, Colors.black),
                    shape: RoundedRectangleBorder(side: BorderSide(
                        color: Colors.blue,
                        width: 1,
                        style: BorderStyle.solid
                    ), borderRadius: BorderRadius.circular(10)),
                  ),
                )
              ],
            ),
          ),
          Row(
            children: [
              Text(widget.userName?.name.toString() ?? 'Profile', style: AppTextStyle.t16B,),
              IconButton(icon: Icon(
                Icons.check_circle_outline,
                color: AppColors.blue,
                size: 25,
              ), onPressed: (){})
            ],
          ),
          Align(
            alignment: Alignment.centerLeft,
              child: Text("Digital Artist/Art Director",
                style: AppTextStyle.t12T,
              )),
          SizedBox(height: 5),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Column(
                  children: [
                    Text("250",style: AppTextStyle.t14B,),
                    Text("captures",style: AppTextStyle.t12N,)
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  children: [
                    Text("1271",style: AppTextStyle.t14B,),
                    Text("followers",style: AppTextStyle.t12N,)
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  children: [
                    Text("475",style: AppTextStyle.t14B,),
                    Text("following",style: AppTextStyle.t12N,)
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  _buildFeaturedPhotos() {
    return Container(
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
              child: Text("Featured Photos",style: AppTextStyle.t14B,)),

          Container(
            height: 100,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
                itemBuilder: (context,index) {
                  return listFeaturedPhotos();
                },
                separatorBuilder: (context,index) => SizedBox(width: 1,),
                itemCount: 5),
          ),
        ],
      ),
    );

  }

  listFeaturedPhotos() {
    return Row(
      children: [
        Image.asset(
            'assets/images/pic1.png',
            fit: BoxFit.fitWidth
        ),

      ],
    );
  }

  _buidAllPhotos() {
    return Column(
      children: [
        Row(
            children:[
              Expanded(
                  flex: 1,
                  child: Text("All Photos",style: AppTextStyle.t14B,)),
              Icon(Icons.grid_view,color: Colors.black,size: 15,),
              Text(" | ",style: AppTextStyle.t16B,),
              Icon(Icons.list,color: Colors.black,size: 17),
        ]),
        Expanded(
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.all(8.0),
            // height: 100,
            child: GridView.count(
              crossAxisCount: 2,
              children: List.generate(10, (index) {
                return Center(
                  child: Image.asset(
                      'assets/images/pic42.png',
                      fit: BoxFit.fill
                  ),
                );
              }),
            ),
          ),
        ),
      ],
    );
  }

}
