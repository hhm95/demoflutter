import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/app/home/widgets/item_feed_home.dart';
import 'package:flutter_app/src/app_router.dart';
import 'package:flutter_app/src/common/app_colors.dart';
import 'package:flutter_app/src/common/app_text_style.dart';
import 'package:flutter_app/src/common/widgets/avatar_circle.dart';
import 'package:flutter_app/src/resouce/app_images.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  var listPerson = ['a', 'b', 'c', 'd','e','f','g','h','i','j','k','d','d'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(context),
      floatingActionButton:
      FloatingActionButton(
          backgroundColor: Colors.deepPurpleAccent,
          child: Icon(Icons.add,color: Colors.white,),
          onPressed: () {

          }),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('Feed', style: AppTextStyle.t26B.copyWith(color: AppColors.dark),),
      actions: [
        IconButton(icon: Icon(Icons.search, color: AppColors.blue,), onPressed: () {

        })
      ],
    );
  }

  Widget _buildBody(BuildContext context) {
    return Column(
      children: [
        _buildHeaderWidget(),
        SizedBox(height: 16,),
        Expanded(child: _buildListFeed(context))
      ],
    );
  }

  Widget _buildHeaderWidget() {
    return Container(
      constraints: BoxConstraints(minHeight: 100),
      padding: EdgeInsets.only(top: 16, left: 16),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(child: Text('Features Stories', style: AppTextStyle.t16B,)),
              Text('See More', style: AppTextStyle.t16N,)
            ],
          ),
          SizedBox(height: 10,),
          Container(
            height: 80,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  AvatarCircle(AppImages.imUserInit, 80),
                  AvatarCircle(AppImages.imUser4, 80),
                  AvatarCircle(AppImages.imUser2, 80),
                  AvatarCircle(AppImages.imUser3, 80),
                  AvatarCircle(AppImages.imUser1, 80),
                ],
              ),
            ),
            // ListView.separated(// list image
            //   scrollDirection: Axis.horizontal,
            //     itemBuilder: (context, index) {
            //         AvatarCircle(AppImages.imUser1, 60);
            //     },
            //     separatorBuilder: (context, index) => Container(width: 8,),
            //     itemCount: listPerson.length),
          )
        ],
      ),
    );
  }

  _buildListFeed(BuildContext context) {
    return ListView.separated(
        scrollDirection: Axis.vertical,
        itemBuilder: (ctx, index) {
          return ItemFeedHome(
            onClickAvatar: (nameAvatar) {
              Navigator.pushNamed(context, AppRouter.userProfileScreen, arguments: Persion(name: nameAvatar, age: 26));
            },
          );
        },
        separatorBuilder: (context, index) => SizedBox(height: 10,),
        itemCount: 5);
  }

}

class Persion {
  String name;
  int age;
  Persion({this.name, this.age});
}







