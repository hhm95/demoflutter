
import 'package:flutter/material.dart';
import 'package:flutter_app/src/app/user/user_profile.dart';

class ItemFeedHome extends StatelessWidget {

  final Function(String) onClickAvatar;
  final Function() onClickFavourite;
  final Function() onClickMore;

  ItemFeedHome({this.onClickAvatar, this.onClickFavourite, this.onClickMore});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Center(
            child: Container(
              child: new Image.asset(
                  'assets/images/pic1.png',
                  fit: BoxFit.fill
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
            child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: InkWell(
                        onTap: () {
                          onClickAvatar('Luna JR');
                        },
                        child: Container(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: Image.asset('assets/images/pic31.png',
                                ),
                          ),),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      child: Column(
                        children: [
                          Text("Luna JR",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, ),),
                          Text("2 hours ago"),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(

                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      height: 70,
                      child: Row(
                        children: [
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: IconButton(
                              icon: Icon(
                                Icons.favorite,
                                color: Colors.red,
                                size: 35,
                              ),
                              onPressed: () {
                                // setState(() {
                                //   _changeColor1 = !_changeColor1;
                                // });
                              },
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.more_vert,
                              // color: Colors.red,
                              size: 35,
                            ),
                            onPressed: () {
                              // setState(() {
                              // });
                            },
                          ),
                        ],
                      ),
                    ),
                  ),

                ]),
          ),
          Center(
            child: Container(
              child: new Image.asset(
                  'assets/images/pic2.png',
                  fit: BoxFit.fill
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
            child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: Container(
                        child: new Image.asset(
                            'assets/images/pic32.png',
                            fit: BoxFit.fitWidth
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      child: Column(
                        children: [
                          Text("Jesica fariya",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16,),),
                          Text("1 hours ago"),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: Container(
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Center(
                      child: Container(
                        height: 70,
                        child: Row(
                          children: [
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: IconButton(
                                icon: Icon(
                                  Icons.favorite,
                                  color: Colors.red,
                                  size: 35,
                                ),
                                onPressed: () {
                                  // setState(() {
                                  //   _changeColor2 = !_changeColor2;
                                  // });
                                },
                              ),
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.more_vert,
                                // color: Colors.red,
                                size: 35,
                              ),
                              onPressed: () {
                                // setState(() {
                                // });
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),

                ]),
          ),
          Center(
            child: Container(
              child: new Image.asset(
                  'assets/images/pic1.png',
                  fit: BoxFit.fill
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
            child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: Container(
                        child: new Image.asset(
                            'assets/images/pic31.png',
                            fit: BoxFit.fitWidth
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      child: Column(
                        children: [
                          Text("Cris Phan",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, ),),
                          Text("5 hours ago"),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(

                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      height: 70,
                      child: Row(
                        children: [
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: IconButton(
                              icon: Icon(
                                Icons.favorite,
                                color: Colors.red,
                                size: 35,
                              ),
                              onPressed: () {
                                // setState(() {
                                //   _changeColor1 = !_changeColor1;
                                // });
                              },
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.more_vert,
                              // color: Colors.red,
                              size: 35,
                            ),
                            onPressed: () {
                              // setState(() {
                              // });
                            },
                          ),
                        ],
                      ),
                    ),
                  ),

                ]),
          ),
        ],
      ),
    );
  }

  void gotoProfile(String s) {
    // Navigator.push(
    //     this.context,
    //     MaterialPageRoute(
    //       builder: (context) => UserProfileScreen(),
    //     ));
  }
}
