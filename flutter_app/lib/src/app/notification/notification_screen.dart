
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/common/app_colors.dart';
import 'package:flutter_app/src/common/app_text_style.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
      bottomNavigationBar: _buiBottomNavigation(),
    );
  }

  _buildAppBar() {
    return AppBar(
      title: Text("Notifications",style: AppTextStyle.t26B.copyWith(color: AppColors.dark),),
      actions: [
        IconButton(icon: Icon(Icons.filter_list_rounded), onPressed: (){

        })
      ],
    );

  }

  _buildBody() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          _buildNotificationToday(),
          SizedBox(height: 10),
          Expanded(child: _buildNotificationYesterday()),

        ],
      ),
    );

  }

  _buildNotificationToday() {
    return Container(
      height: 200,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(18, 18, 0, 0),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text("Today",style: AppTextStyle.t14B,)),
          ),
          SizedBox(height: 10,),
          Expanded(
            child: ListView.separated(
                itemBuilder: (context, index){
                  return listNotificationsToday();
                },
                separatorBuilder: (context, index) => SizedBox(height: 5,),
                itemCount: 10),
          )
        ],
      ),

    );
  }

  listNotificationsToday() {
    return Row(
      children: [
        Expanded(
            flex: 3,
            child: IconButton(icon: Icon(Icons.message_outlined, color: AppColors.blue,), onPressed: (){})),
        Expanded(
          flex: 12,
          child: Column(
            children: [
              Text("Hoang Huu Manh commrnted on your photo \"Manh Hoang Huu\"",style: AppTextStyle.t12N,),
              Row(
                children: [
                  Icon(Icons.access_time,size: 15 ,),
                  SizedBox(width: 5,),
                  Text("4 mins ago",style: AppTextStyle.t12T,)
                ],
              ),
            ],
          ),
        ),
        Expanded(
            flex: 1,
            child: IconButton(icon: Icon(Icons.more_vert), onPressed: (){}))
      ],
    );
  }

  _buildNotificationYesterday() {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(18, 18, 0, 0),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text("Yesterday",style: AppTextStyle.t14B,)),
          ),
          SizedBox(height: 10,),
          Container(
            child: Expanded(
              child: ListView.separated(
                  itemBuilder: (context, index){
                    return listNotificationsToday();
                  },
                  separatorBuilder: (context, index) => SizedBox(height: 5,),
                  itemCount: 15),
            ),
          )
        ],
      ),

    );
  }

  _buiBottomNavigation() {
    return BottomAppBar(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(icon: Icon(Icons.home), onPressed: () {}),
            IconButton(icon: Icon(Icons.mark_email_unread), onPressed: () {}),
            IconButton(icon: Icon(Icons.add_alert), onPressed: () {}),
            IconButton(icon: Icon(Icons.person), onPressed: () {}),
          ],
        ),
      ),
    );
  }
}
