import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/app/home/home_screen.dart';
import 'package:flutter_app/src/app/user/user_profile.dart';
import 'package:flutter_app/src/app_navigation.dart';

class AppRouter {

  //key screen
  static const homeScreen = '/homeScreen';
  static const message = '/message';
  static const notification = '/notification';
  static const userProfileScreen = '/UserProfileScreen';
  static const appNavigator = '/AppNavigator';


  //oke
  static Map<String, Widget Function(BuildContext)> generateRouter() {
    //screen ko co param truyen sang thi define o day!!!
    return {
      AppRouter.appNavigator: (context) => AppNavigation(),
    };
  }

  static Route<dynamic> onGenerateRoute(RouteSettings setting, BuildContext context) {
    //screen nao co param truyen sang thi define o day !!!

    switch (setting.name) {
      case AppRouter.userProfileScreen:
        Persion persion = setting.arguments as Persion;
        return MaterialPageRoute(builder: (context){
          return UserProfileScreen(userName: persion,);
        });
      default:
        Persion persion = setting.arguments as Persion;
        return MaterialPageRoute(builder: (context){
          return UserProfileScreen(userName: persion,);
        });
    }
  }

}
