import 'package:flutter/material.dart';
import 'package:flutter_app/src/app/home/home_screen.dart';
import 'package:flutter_app/src/app/notification/notification_screen.dart';
import 'package:flutter_app/src/app/user/user_profile.dart';
import 'package:flutter_app/src/app_navigation.dart';
import 'package:flutter_app/src/app_router.dart';
import 'package:flutter_app/src/resources/login_page.dart';
import 'package:flutter_app/src/resources/view_chart.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),//LoginPage //ViewChart
      builder: EasyLoading.init(),
      routes: AppRouter.generateRouter(),
      initialRoute: AppRouter.homeScreen,
      onGenerateRoute: (setting) {
        return AppRouter.onGenerateRoute(setting, context);
      },
    );
  }
}

//
