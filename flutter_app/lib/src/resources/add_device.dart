// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_app/src/blocs/login_bloc.dart';
import 'package:flutter_app/src/blocs/newdevice_bloc.dart';
import 'package:flutter_app/src/others/EasyJsonParse/Users.dart';
import 'package:flutter_app/src/resouce/app_images.dart';
import 'package:flutter_app/src/resources/home_page.dart';
import 'package:flutter_app/src/resources/view_device.dart';
import 'dart:developer';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'login_page.dart';

// ignore: must_be_immutable
class AddDevice extends StatefulWidget {

  String username;
  String password;
  AddDevice({this.username, this.password});

  @override
  _AddDeviceState createState() => _AddDeviceState(username, password);
}

class _AddDeviceState extends State<AddDevice> {

  String username;
  String password;
  AddNewDeviceBloc bloc = new AddNewDeviceBloc();
  TextEditingController _imeiController = new TextEditingController();
  TextEditingController _deviceKeyController = new TextEditingController();
  TextEditingController _deviceNameController = new TextEditingController();
  _AddDeviceState(this.username, this.password);

  @override

  void initState() {
    // TODO: implement initState
    //goi api
    // EasyLoading.showToast(username);
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement buil;
    return Scaffold(
      appBar: AppBar(
        title: Text("Thêm thiết bị"),
      ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Container(
            width: MediaQuery.of(context).size.width,
            // height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
            //constraints: BoxConstraints.expand(),
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 20,),
                Center(
                  child: Container(
                    width: 250,
                    // height: 200,
                    child: new Image.asset(
                        AppImages.imLog,
                        fit: BoxFit.fitWidth
                    ),
                  ),
                ),
                SizedBox(height: 30,),
                Center(child: Text(
                    "Thiết bị mới",
                    style: TextStyle(fontSize: 30))),

                SizedBox(height: 20,),
                Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                    child: StreamBuilder(
                        stream: bloc.imeiStream,
                        builder: (context, AsyncSnapshot<String>snapshot) {
                          String error;
                          if (snapshot.hasData) {
                            error = snapshot.data;
                          }
                          return TextField(
                            style: TextStyle(fontSize: 18, color: Colors.black),
                            controller: _imeiController,
                            decoration: InputDecoration(
                                labelText: "S/N",
                                errorText: error,
                                labelStyle:
                                TextStyle(
                                    color: Color(0xff888888), fontSize: 15)),
                          );
                        })),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                    child: StreamBuilder(
                      stream: bloc.deviceKeyStream,
                      builder: (context, AsyncSnapshot<String>snapshot) {
                        String error;
                        if (snapshot.hasData) {
                          error = snapshot.data;
                        }
                        return TextField(
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          controller: _deviceKeyController,
                          decoration: InputDecoration(
                              labelText: "DeviceKey",
                              errorText: error,
                              labelStyle:
                              TextStyle(
                                  color: Color(0xff888888), fontSize: 15)),
                        );
                      },
                    ),
                ),
                // Padding(
                //     padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                //     child: StreamBuilder(
                //         stream: bloc.softwareKeyStream,
                //         builder: (context, AsyncSnapshot<String>snapshot) {
                //           String error;
                //           if (snapshot.hasData) {
                //             error = snapshot.data;
                //           }
                //           return TextField(
                //             style: TextStyle(fontSize: 18, color: Colors.black),
                //             controller: _softwareKeyController,
                //             decoration: InputDecoration(
                //                 labelText: "SoftwareKey",
                //                 errorText: error,
                //                 labelStyle:
                //                 TextStyle(
                //                     color: Color(0xff888888), fontSize: 15)),
                //           );
                //         })),
                Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                    child: StreamBuilder(
                        stream: bloc.deviceNameStream,
                        builder: (context, AsyncSnapshot<String>snapshot) {
                          String error;
                          if (snapshot.hasData) {
                            error = snapshot.data;
                          }
                          return TextField(
                            style: TextStyle(fontSize: 18, color: Colors.black),
                            controller: _deviceNameController,
                            decoration: InputDecoration(
                                labelText: "Tên thiết bị",
                                errorText: error,
                                labelStyle:
                                TextStyle(
                                    color: Color(0xff888888), fontSize: 15)),
                          );
                        })),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: SizedBox(
                    width: double.infinity,
                    height: 56,
                    child: RaisedButton(
                      color: Colors.blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      onPressed: onAddDeviceClicked,
                      child: Text("Thêm", style: TextStyle(
                          color: Colors.white,
                          fontSize: 16),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
  }

  void onAddDeviceClicked() async {
    final String imei = _imeiController.text;
    final String deviceKey = _deviceKeyController.text;
    final String deviceName = _deviceNameController.text;
    bloc.addNewDevice(username, imei, deviceKey, deviceName).then((error){
      if(error != null){
        EasyLoading.showToast(error);
      }else{
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ViewDevice(username: username),
            ));
      }
    });
  }

}


