// import 'dart:html';

import 'dart:io';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/environment.dart';
import 'package:flutter_app/src/blocs/login_bloc.dart';
import 'package:flutter_app/src/common/app_colors.dart';
import 'package:flutter_app/src/others/EasyJsonParse/Users.dart';
import 'package:flutter_app/src/resources/home_page.dart';
import 'package:flutter_app/src/resources/view_device.dart';

import 'dart:developer';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'signup.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  //User _user;
  LoginBloc bloc = new LoginBloc();
  bool _showPass = false;
  bool _showLienKet = false;
  bool _changeColor1 = false;
  bool _changeColor2 = false;
  String _counter = '';
  TextEditingController _userController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();
  TextEditingController _linkController = new TextEditingController(text: 'http://117.1.17.14:7500/');

  bool checkBoxValue = false;
  bool _save_pass = false;
  bool hasConnection = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadCounter();
    //internet

  }

  //Loading counter value on start
  _loadCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _counter = (prefs.getString('counter1') ?? '');
      if(_counter.length > 0){
        // print('data counter: $_counter');
        // print('has : data');
        checkBoxValue = true;
      }
      _userController.text = _counter.split("_").first;
      _passController.text = _counter.split("_").last;
    });

    //print('data counter: $_counter');
  }
  //Incrementing counter after click
  _incrementCounter(String username, String password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      // _counter = (prefs.getString('counter1') ?? '0') + username  + '_' + password;
      _counter = username  + '_' + password;
      prefs.setString('counter1', _counter);
    });
  }
  _resetCounter() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _counter = '';
      prefs.setString('counter1', _counter);
    });
    //print('reset counter: $_counter');
  }

  @override
  void dispose() {
    // TODO: implement dispose
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement buil;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Container(
            width: MediaQuery.of(context).size.width,
            // height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
            //constraints: BoxConstraints.expand(),
            color: AppColors.blue,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(18, 38, 18, 18),
                child: Container(
                  child: Text("Feed",style: TextStyle(fontSize: 28, color: Colors.black, fontWeight: FontWeight.bold),),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(18, 18, 18, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text("Featured Stories,",style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold),),
                    ),
                    Container(
                      width: 40,
                      alignment: Alignment.centerRight,
                      child: Center(
                        child: Container(
                          child: new Image.asset(
                              'assets/images/search.png',
                              fit: BoxFit.fitWidth
                          ),
                        ),
                      ),
                    ),
                  ]),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                        setState(() {

                        });
                      },
                      child: Container(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20.0),
                          child: Image.asset('assets/images/initpic.png',
                            width: 110.0, height: 110.0),
                      ),),
                  ),
                    // Center(
                    //   child: Container(
                    //     child: new Image.asset(
                    //         'assets/images/initpic.png',
                    //         fit: BoxFit.fitWidth
                    //     ),
                    //   ),
                    // ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: Container(
                        child: new Image.asset(
                            'assets/images/pic31.png',
                            fit: BoxFit.fitWidth
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: Container(

                        child: new Image.asset(
                            'assets/images/pic32.png',
                            fit: BoxFit.fitWidth
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: Container(
                        child: new Image.asset(
                            'assets/images/pic33.png',
                            fit: BoxFit.fitWidth
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: Container(
                        child: new Image.asset(
                            'assets/images/pic34.png',
                            fit: BoxFit.fitWidth
                        ),
                      ),
                    ),
                  ),
                ]),
              ),
              Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Center(
                        child: Container(
                          child: new Image.asset(
                              'assets/images/pic1.png',
                              fit: BoxFit.fill
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Center(
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      gotoProfile("Luna JR");
                                    });
                                  },
                                  child: Container(
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20.0),
                                      child: Image.asset('assets/images/pic31.png',
                                          width: 110.0, height: 110.0),
                                    ),),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Container(
                                child: Column(
                                  children: [
                                    Text("Luna JR",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, ),),
                                    Text("2 hours ago"),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(

                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Container(
                                height: 70,
                                child: Row(
                                  children: [
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.favorite,
                                          color: _changeColor1? Colors.grey : Colors.red,
                                          size: 35,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            _changeColor1 = !_changeColor1;
                                          });
                                        },
                                      ),
                                    ),
                                    IconButton(
                                      icon: Icon(
                                        Icons.more_vert,
                                        // color: Colors.red,
                                        size: 35,
                                      ),
                                      onPressed: () {
                                        setState(() {
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),

                          ]),
                      ),
                      Center(
                        child: Container(
                          child: new Image.asset(
                              'assets/images/pic2.png',
                              fit: BoxFit.fill
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
                        child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Center(
                                  child: Container(
                                    child: new Image.asset(
                                        'assets/images/pic32.png',
                                        fit: BoxFit.fitWidth
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                  child: Column(
                                    children: [
                                      Text("Jesica fariya",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16,),),
                                      Text("1 hours ago"),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Center(
                                  child: Container(
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Center(
                                  child: Container(
                                    height: 70,
                                    child: Row(
                                      children: [
                                        Align(
                                          alignment: Alignment.bottomCenter,
                                          child: IconButton(
                                            icon: Icon(
                                              Icons.favorite,
                                              color: _changeColor2? Colors.grey : Colors.red,
                                              size: 35,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                _changeColor2 = !_changeColor2;
                                              });
                                            },
                                          ),
                                        ),
                                        IconButton(
                                          icon: Icon(
                                            Icons.more_vert,
                                            // color: Colors.red,
                                            size: 35,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),

                            ]),
                      ),
                      Center(
                        child: Container(
                          child: new Image.asset(
                              'assets/images/pic1.png',
                              fit: BoxFit.fill
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
                        child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Center(
                                  child: Container(
                                    child: new Image.asset(
                                        'assets/images/pic31.png',
                                        fit: BoxFit.fitWidth
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                  child: Column(
                                    children: [
                                      Text("Cris Phan",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, ),),
                                      Text("5 hours ago"),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Container(

                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                  height: 70,
                                  child: Row(
                                    children: [
                                      Align(
                                        alignment: Alignment.bottomCenter,
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.favorite,
                                            color: _changeColor1? Colors.grey : Colors.red,
                                            size: 35,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              _changeColor1 = !_changeColor1;
                                            });
                                          },
                                        ),
                                      ),
                                      IconButton(
                                        icon: Icon(
                                          Icons.more_vert,
                                          // color: Colors.red,
                                          size: 35,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                          });
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ),

                            ]),
                      ),
                     ],
                  ),
                ),


              SizedBox(
                height: 100,
             ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(icon: Icon(Icons.home), onPressed: () {}),
                IconButton(icon: Icon(Icons.mark_email_unread), onPressed: () {}),
                IconButton(icon: Icon(Icons.add_alert), onPressed: () {}),
                IconButton(icon: Icon(Icons.person), onPressed: () {}),
            ],
            ),
          ),
        ),
        floatingActionButton:
        FloatingActionButton(
          backgroundColor: Colors.deepPurpleAccent,
            child: Icon(Icons.add,color: Colors.white,),
            onPressed: () {

            }),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),

    );
  }

  void onToggleShowPass() {
    setState(() {
      _showPass = !_showPass;
    });
  }

  void onSignInClicked() async {

    // checkInternet();
    // ConnectionStatusSingleton connectionStatus = ConnectionStatusSingleton.getInstance();
    // connectionStatus.initialize();

    // var isConnect = checkConnection();
    // print('value = $isConnect.');
    // if(isConnect == Future<bool>.value(true)){
    //   print('connect');
    // }else{
    //   print('no con');
    // }
    // print('...abc');
    //
    final String username = _userController.text;
    final String password = _passController.text;
    final String link = _linkController.text;

    setState(() {
      _save_pass = checkBoxValue;
    });

    // print('value save =: $_save_pass');

    //check valid string http;
    if (_linkController.text.isEmpty) {
      return;
    }
    var array = _linkController.text.split(":");
    if (array.length < 3) {
      return;
    }
    Environment.IP = array[1].replaceAll("//", "");
    Environment.PORT = array[2].replaceAll("/", "");
    //print('data x: ${Environment.IP+ "  "+Environment.PORT}');
    //check internet
    isInternet();
    //
    bloc.login(username, password, link).then((error) {
      if (error != null) {
        EasyLoading.showToast(error);
      } else {
        // Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //       builder: (context) => ViewDevice(username: bloc.user.email,password: bloc.user.password),
        //     ));

        if(_save_pass){
          _incrementCounter(username, password);
        }else{
          _resetCounter();
        }

        // print('data counter: $_counter');
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) =>
            ViewDevice(username: bloc.user.email)
        ));
      }
    });
  }

  void gotoSignUpClicked() {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SignUpPage(),
        ));
  }

  Widget gotoHome(BuildContext context) {
    return HomePage();
  }

  void gotoProfile(String username) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SignUpPage(),
        ));
  }
}
