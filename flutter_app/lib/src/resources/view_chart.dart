import 'dart:ffi';
// import 'dart:html';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/src/blocs/login_bloc.dart';
import 'package:flutter_app/src/blocs/viewdevice_bloc.dart';
import 'package:flutter_app/src/validators/mapErrCode.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/blocs/newdevice_bloc.dart';
import 'package:flutter_app/src/blocs/viewchart_bloc.dart';
import 'package:flutter_app/src/blocs/viewinfo_bloc.dart';
import 'package:flutter_app/src/others/EasyJsonParse/GetDays.dart';
import 'package:flutter_app/src/others/EasyJsonParse/UserViewDevice.dart';
import 'package:flutter_app/src/others/EasyJsonParse/ViewChart.dart';
import 'package:flutter_app/src/others/EasyJsonParse/ViewInfo.dart';
import 'package:flutter_app/src/resources/information_page.dart';
import 'package:flutter_app/src/resources/view_device.dart';
import 'dart:developer';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'login_page.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:charts_flutter/src/text_element.dart';
import 'package:charts_flutter/src/text_style.dart' as style;
import 'package:flutter_app/src/blocs/viewdevice_bloc.dart';
// ignore: must_be_immutable
class ViewChart extends StatefulWidget {
  String imei;
  String password;
  String username;
  ListElement datas;
  UserViewDevice value1;

  ViewChart({this.imei, this.username, this.datas,});

  @override
  _ViewChartState createState() =>
      _ViewChartState(imei, username, datas);
}

class CustomCircleSymbolRenderer extends CircleSymbolRenderer {
  @override
  void paint(ChartCanvas canvas, Rectangle<num> bounds,
      {List<int> dashPattern,
      Color fillColor,
      Color strokeColor,
      double strokeWidthPx}) {
    super.paint(canvas, bounds,
        dashPattern: dashPattern,
        fillColor: fillColor,
        strokeColor: strokeColor,
        strokeWidthPx: strokeWidthPx);

    canvas.drawRect(
        Rectangle(bounds.left - 5, bounds.top - 30, bounds.width + 10,
            bounds.height + 10),
        fill: Color.white);
    var textStyle = style.TextStyle();
    textStyle.color = Color.black;
    textStyle.fontSize = 15;
    canvas.drawText(TextElement(AppStaticVar.valueDisplay, style: textStyle),
        (bounds.left).round(), (bounds.top - 28).round());
  }
}

class _ViewChartState extends State<ViewChart> {
  String radioButtonItem = 'ONE';
  UserViewDevice value1;
  LoginBloc bloc2 = new LoginBloc();

  // Group Value for Radio Button.
  int id = 1;
  ViewDeviceBloc bloc1 = new ViewDeviceBloc();

  List<String> _status = ["Pending", "Released", "Blocked"];
  String imei;
  String username;
  ListElement datas;
  List<Listerr> errors = [];
  List<Listerr> errors2 = [];
  String textSelected;
  String _selection;
  ViewChartBloc bloc = new ViewChartBloc();
  List<DataChart> value;
  ListElement value_capnhat;
  UserViewDevice value2;
  ViewInfoBloc blocInfo = new ViewInfoBloc();
  List<ViewInfo> infos;
  String name;
  String valuePin;
  String dir;
  String radioItem = '';
  String textChart = '';

  // var error_ime=[];

  // double meanValue = 0.0;
  List<charts.Series> seriesList;
  int _radioValue = 0;
  int _radio1 = 0;

  final bool animate = false;
  bool is_sanluong = false;
  bool viewThangClicked = false;
  bool viewNamClicked = false;
  bool viewTuychonClicked = false;
  bool viewNgayClicked = false;
  bool is_test = false;
  double value_tongsanluong=0;
  // var desktopSalesData=[];
  // get desktopSalesData => null;
  var desktopSalesData = [
    DataShow("0", 0),
  ];

  List<charts.Series<DataShow, String>> _createRadomData() {
    // final random = Random();

    return [
      charts.Series<DataShow, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (DataShow data, _) => data.time,
        measureFn: (DataShow data, _) => data.db,
        data: desktopSalesData,
        labelAccessorFn: (DataShow data, _) => '\ ${data.db.toString()}',
      ),
      // charts.Series<DataShow, String>(
      //   id: 'Mobile',
      //   colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
      //   domainFn: (DataShow data, _) => data.time,
      //   measureFn: (DataShow data, _) => data.db,
      //   data: desktopSalesData,
      //   labelAccessorFn: (DataShow data, _) => '\$${data.db.toString()}',
      // )..setAttribute(charts.rendererIdKey, "customLine")
    ];
  }


  void _handleRadioValueChange(int value) async{
    if(await isInternet() == false) return;
    setState(() {
      _radioValue = value;
      addSanLuong(
          _selection,
          _radioValue.toString(),
          _radio1.toString());
      switch (_radioValue) {
        case 0:
          {
            //EasyLoading.showToast("E_today");
            is_sanluong = false;
            // is_test=false;

          }

          break;
        case 1:
          {
            //EasyLoading.showToast("TTGHD");
            is_sanluong = false;
            // is_test=false;

          }
          break;
        case 2:
          {
            //EasyLoading.showToast("SLLK");
            is_sanluong = false;
            // is_test=false;
          }
          break;
        case 3:
          {
            //EasyLoading.showToast("SL");
            is_sanluong = true;
            // is_test=false;
          }
          break;
        case 4:
          {
            //EasyLoading.showToast("SL");
            is_sanluong = false;
            // is_test=false;
          }
          break;
      }
    });
  }

  void _handleRadioValueChange1(int value) async {
    if(await isInternet() == false) return;
    setState(() {
      _radio1 = value;
      addSanLuong(
          _selection,
          _radioValue.toString(),
          _radio1.toString());
      switch (_radio1) {
        case 0:
          {
            //EasyLoading.showToast("E_today");
            // is_sanluong = false;
            // is_test=false;

          }

          break;
        case 1:
          {
            //EasyLoading.showToast("TTGHD");
            // is_sanluong = false;
            // is_test=false;

          }
          break;
        case 2:
          {
            //EasyLoading.showToast("SLLK");
            // is_sanluong = false;
            // is_test=false;
          }
          break;
      }
    });
  }

  final List<String> list_choice = [
    'eToday',
    'pIn',
    'pOut',
    'pMaxtoday',
    'vGrid',
    'iGrid',
    'fGrid',
    'powerFactor',
    'vPv1',
    'iPv1',
    'vPv2',
    'iPv2',
    'iLeakage',
    'sllk',
    'ttghd',
    'time',
    'errcode'
  ];

  _ViewChartState(
      this.imei, this.username, this.datas);

  DateTime _dateFrom = DateTime.now();
  DateTime _dateTo = DateTime.now();

  bool _isShowFilter = false;
  bool show_btn = true;

  bool checkedValue = true;

  bool onClickCauHinh = false;
  bool onClickSanluong = false;
  bool onClickDothi = true;
  int _index = 1;

  final List<ChartData> chartData = [
    // ChartData('2010', 35),
    // ChartData('2011', 28),
  ];

  @override
  void initState() {

    // TODO: implement initState


    // getInfo(imei);

    getValue(username); //get 3 data VTSL

    //print('error: ${errors2.length}');
    seriesList = _createRadomData();
    //view default:
    addItemToList(_selection);

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    blocInfo.dispose();
    bloc.dispose();
    bloc1.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement buil;
    // Widget child = Container();
    // switch(_index) {
    //   case 0:
    //     child = FlutterLogo();
    //     break;
    //
    //   case 1:v
    //     child = FlutterLogo();
    //     break;
    // }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.datas.name),
        actions: <Widget>[
          (onClickDothi == true)
              //cauhinh?null : dothi? lít 14ca : sanlung? lít 4 : null
              ? PopupMenuButton(
                  icon: Icon(Icons.bar_chart),
                  onSelected: (String result)async {
                    if(await isInternet() == false) return;
                    setState(() {
                      _selection = result;
                      // print('$_selection');
                      addItemToList(_selection);
                    });
                  },
                  itemBuilder: (context) => [
                    // PopupMenuItem(
                    //   value: "E_today",
                    //   child: Text("E_today"),
                    // ),
                    PopupMenuItem(
                      value: "P_in(W)",
                      child: Text("P_in"),
                    ),
                    PopupMenuItem(
                      value: "P_out(W)",
                      child: Text("P_out"),
                    ),
                    // PopupMenuItem(
                    //   value: "P_max(W)",
                    //   child: Text("P_max"),
                    // ),
                    PopupMenuItem(
                      value: "V_grid(V)",
                      child: Text("V_grid"),
                    ),
                    PopupMenuItem(
                      value: "I_grid(A)",
                      child: Text("I_grid"),
                    ),
                    PopupMenuItem(
                      value: "F_grid(Hz)",
                      child: Text("F_grid"),
                    ),
                    PopupMenuItem(
                      value: "PF",
                      child: Text("PF"),
                    ),
                    PopupMenuItem(
                      value: "V_PV1(V)",
                      child: Text("V_PV1"),
                    ),
                    PopupMenuItem(
                      value: "I_PV1(A)",
                      child: Text("I_PV1"),
                    ),
                    PopupMenuItem(
                      value: "V_PV2(V)",
                      child: Text("V_PV2"),
                    ),
                    PopupMenuItem(
                      value: "I_PV2(A)",
                      child: Text("I_PV2"),
                    ),
                    PopupMenuItem(
                      value: "I_leakage(mA)",
                      child: Text("I_leakage"),
                    ),
                    PopupMenuItem(
                      value: "Temp(oC)",
                      child: Text("Temp"),
                    ),
                    // PopupMenuItem(
                    //   value: "SLLK",
                    //   child: Text("SLLK"),
                    // ),
                    // PopupMenuItem(
                    //   value: "TTGHD",
                    //   child: Text("TTGHD"),
                    // ),
                  ],
                )
              : SizedBox(),
        ],

        // automaticallyImplyLeading: true,
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   currentIndex: _index,
      //   onTap: (int index) => setState(() => _index = index), // this will be set when a new tab is tapped
      //   items: [
      //     BottomNavigationBarItem(
      //       icon: new Icon(Icons.account_box_outlined),
      //       title: new Text('Thông tin'),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: new Icon(Icons.bar_chart),
      //       title: new Text('Xem đồ thị'),
      //     ),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.analytics_outlined),
      //         title: Text('Thống kê')
      //     )
      //   ],
      // ),
      body: Container(
        padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0.0),
        constraints: BoxConstraints.expand(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      onClickCauHinh
                          ? Column(
                              children: <Widget>[
                                Center(
                                  child: Text(
                                    'Thông tin',
                                    style: TextStyle(
                                      fontSize: 34,
                                    ),
                                  ),
                                ),
                                Container(
                                  // width: 300,
                                  child: Padding(
                                    padding: const EdgeInsets.all(18.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Colors.black,
                                        ),
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 1,
                                              child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          0, 15, 0, 15),
                                                  child: StreamBuilder(
                                                      stream: blocInfo.streamInfo,
                                                      builder: (context,
                                                          AsyncSnapshot<
                                                                  List<ViewInfo>>
                                                              snapshot) {
                                                        if (!snapshot.hasData) {
                                                          return Container();
                                                        }
                                                        return Container(
                                                          child: Column(
                                                            children: [
                                                              Padding(
                                                                padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                                                                child: Container(
                                                                  child: Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                                    children: [
                                                                      Row(
                                                                        children: [
                                                                          Expanded(
                                                                            child: Text("S/N: ",
                                                                              style: TextStyle(fontSize: 18,color: Colors.black,),),
                                                                          ),
                                                                          Expanded(
                                                                            child: Container(
                                                                              child:
                                                                              new Text(infos[0].imei,
                                                                                style: TextStyle(
                                                                                    color: Colors.black, fontSize: 18),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: [
                                                                          Expanded(
                                                                            child: Text("Mã sản phẩm: ",
                                                                              style: TextStyle(fontSize: 18,color: Colors.black,),),
                                                                          ),
                                                                          Expanded(
                                                                          child: Container(
                                                                            child:
                                                                                new Text(infos[0].name,
                                                                              style: TextStyle(
                                                                                  color: Colors.black, fontSize: 18),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                          children: [
                                                                            Expanded(
                                                                              child: Text("Hướng góc:",
                                                                                style: TextStyle(fontSize: 18,color: Colors.black,),),
                                                                            ),
                                                                            Expanded(
                                                                          child: Container(child: new Text(infos[0].direction,
                                                                              style: TextStyle(color: Colors.black,
                                                                                  fontSize: 18),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: [
                                                                          Expanded(
                                                                            child: Text("Số lượng pin: ",
                                                                              style: TextStyle(fontSize: 18,color: Colors.black,),),
                                                                          ),
                                                                        Expanded(
                                                                          child: Container(
                                                                            child: new Text(
                                                                              infos[0].valuePin,
                                                                              style: TextStyle(color: Colors.black, fontSize: 18),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: [
                                                                          Expanded(
                                                                            child: Text("Vout(VAC): ",
                                                                              style: TextStyle(fontSize: 18,color: Colors.black,),),
                                                                          ),
                                                                        Expanded(
                                                                          child:
                                                                              Container(
                                                                            child:
                                                                            new Text(infos[0].vOut,
                                                                              style: TextStyle(
                                                                                  color: Colors.black, fontSize: 18),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: [
                                                                          Expanded(
                                                                            child: Text("Vin max(VDC): ",
                                                                              style: TextStyle(fontSize: 18,color: Colors.black,),),
                                                                          ),
                                                                        Expanded(
                                                                          child:
                                                                              Container(
                                                                            child:
                                                                                new Text(
                                                                              infos[0]
                                                                                  .vOutMax,
                                                                              style: TextStyle(
                                                                                  color:
                                                                                      Colors.black,
                                                                                  fontSize: 18),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: [
                                                                          Expanded(
                                                                            child: Text("Hiệu suất đỉnh(%): ",
                                                                              style: TextStyle(fontSize: 18,color: Colors.black,),),
                                                                          ),
                                                                        Expanded(
                                                                          child:
                                                                              Container(
                                                                            child:
                                                                                new Text(
                                                                              infos[0]
                                                                                  .performance,
                                                                              style: TextStyle(
                                                                                  color:
                                                                                      Colors.black,
                                                                                  fontSize: 18),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: [
                                                                          Expanded(
                                                                            child: Text("Phiên bản: ",
                                                                              style: TextStyle(fontSize: 18,color: Colors.black,),),
                                                                          ),
                                                                        Expanded(
                                                                          child: Container(
                                                                            child: new Text(
                                                                              value_capnhat == null ? "...": infos[0].sfVsDevice,
                                                                              style: TextStyle(color: Colors.black, fontSize: 18),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: [
                                                                          Expanded(
                                                                            child: Text("TTG(giờ): ",
                                                                              style: TextStyle(fontSize: 18,color: Colors.black,),),
                                                                          ),
                                                                        Expanded(
                                                                          child:
                                                                              Container(
                                                                            child: new Text(
                                                                              value_capnhat == null ? "...": value_capnhat.ttg,
                                                                              style: TextStyle(color: Colors.black,fontSize: 18),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: [
                                                                          Expanded(
                                                                            child: Text("SLLK(kWh): ",
                                                                              style: TextStyle(fontSize: 18,color: Colors.black,),),
                                                                          ),
                                                                        Expanded(
                                                                          child: Container(child:
                                                                                new Text(
                                                                                  value_capnhat == null ? "...": value_capnhat.sllk,
                                                                              style: TextStyle(
                                                                                  color: Colors.black, fontSize: 18),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: [
                                                                          Expanded(
                                                                            child: Text("SL ngày(kWh): ",
                                                                              style: TextStyle(fontSize: 18,color: Colors.black,),),
                                                                          ),
                                                                        Expanded(
                                                                          child:
                                                                              Container(
                                                                            child:
                                                                                new Text(
                                                                                  value_capnhat == null ? "...": value_capnhat.etoday,
                                                                              style: TextStyle(
                                                                                  color:
                                                                                      Colors.black,
                                                                                  fontSize: 18),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        ],
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        );
                                                      })),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  // alignment: Alignment.centerLeft,
                                  margin:
                                      const EdgeInsets.fromLTRB(0, 5, 0, 0),
                                  // color: const Color(0xff),
                                  child: Text(
                                    "Lịch sử lỗi",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontSize: 34, color: Colors.black),
                                  ),
                                ),
                                Container(
                                  margin:
                                      const EdgeInsets.fromLTRB(20, 15, 20, 2),
                                  color: Colors.blue[500],
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Expanded(
                                          flex: 3,
                                          child: Text(
                                            'Ngày',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                            ),
                                          )),
                                      Container(
                                        height: 40,
                                        width: 2.0,
                                        color: Colors.white,
                                      ),
                                      Expanded(
                                          flex: 3,
                                          child: Text(
                                            'Giờ',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                            ),
                                          )),
                                      Container(
                                        height: 40,
                                        width: 2.0,
                                        color: Colors.white,
                                      ),
                                      Expanded(
                                          flex: 2,
                                          child: Text(
                                            'Mã lỗi',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(19.0, 0, 19, 0),
                                  child: Container(
                                    height: errors2.length < 1
                                        ? 50.0
                                        : ((50 + 51 * errors2.length) >
                                                MediaQuery.of(context)
                                                        .size
                                                        .height /
                                                    4)
                                            ? MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                4
                                            : (50 + 51 * errors2.length)
                                                .toDouble(),
                                    constraints: BoxConstraints(
                                        maxHeight:
                                            MediaQuery.of(context).size.height /
                                                3),
                                    child: CupertinoScrollbar(

                                      child: ListView.builder(
                                          itemCount: errors2.length,
                                          itemBuilder: (context, index) {
                                            return Container(
                                              height: 50,
                                              margin: const EdgeInsets.fromLTRB(
                                                  0, 1, 0, 1),
                                              color: Colors.blue[200],
                                              //Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(0.6),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.max,
                                                children: [
                                                  Expanded(
                                                      flex: 3,
                                                      child: Container(
                                                        margin: const EdgeInsets
                                                                .fromLTRB(
                                                            10, 0, 5, 0),
                                                        child: Center(
                                                          child: Text(
                                                              errors2[index].time.substring(0,8), //.imei.substring(13, 19),
                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color: Colors
                                                                    .black,
                                                              )),
                                                        ),
                                                      )),
                                                  Container(
                                                    height: 50,
                                                    width: 2.0,
                                                    color: Colors.white,
                                                  ),
                                                  Expanded(
                                                      flex: 3,
                                                      child: Container(
                                                        margin: const EdgeInsets
                                                            .fromLTRB(
                                                            10, 0, 5, 0),
                                                        child: Center(
                                                          child: Text(
                                                              errors2[index].time.substring(9,17),
                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color: Colors
                                                                    .black,
                                                              )),
                                                        ),
                                                      )),
                                                  // Container(
                                                  //   height: 50,
                                                  //   width: 2.0,
                                                  //   color: Colors.white,
                                                  // ),
                                                  Container(
                                                    height: 50,
                                                    width: 2.0,
                                                    color: Colors.white,
                                                  ),
                                                  Expanded(
                                                      flex: 2,
                                                      child: Container(
                                                          margin:
                                                          const EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                          child: FlatButton(
                                                            onPressed: () {
                                                              _showDialog2(errors2[index].errcode);
                                                            },
                                                              child: Text(
                                                                errors2[index].errcode,
                                                                textAlign:
                                                                TextAlign.center,
                                                                style: TextStyle(
                                                                  fontSize: 16,
                                                                ),
                                                              )
                                                          ))),
                                                  // Expanded(
                                                  //     flex: 2,
                                                  //     child: Text(
                                                  //       errors2[index].errcode,
                                                  //       textAlign:
                                                  //           TextAlign.center,
                                                  //       style: TextStyle(
                                                  //         fontSize: 16,
                                                  //       ),
                                                  //     )),
                                                ],
                                              ),
                                            );
                                          }),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      // Center(
                                      //   child: IconButton(icon: Icon(Icons.refresh), onPressed: () {
                                      //     // deleteDevice(username, imei, password);
                                      //     getValue(username);
                                      //   },
                                      //   ),
                                      // ),
                                      Expanded(
                                        flex: 1,
                                        child: Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                40, 5, 40, 10),
                                            child: IconButton(icon: Icon(Icons.refresh), onPressed: () {
                                              getValue(username);
                                            },
                                            )
                                        ),
                                      ),
                                      // Expanded(
                                      //   flex: 1,
                                      //   child: Padding(
                                      //     padding: EdgeInsets.fromLTRB(
                                      //         40, 5, 40, 10),
                                      //     child: (username == "admin")
                                      //         ? IconButton(icon: Icon(Icons.edit_sharp), onPressed: () {
                                      //                 onEditClick(
                                      //                     infos, username);
                                      //               },)
                                      //         : Container(
                                      //             height: 40,
                                      //           ),
                                      //   ),
                                      // ),
                                      Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              40, 5, 40, 10),
                                          child: IconButton(icon: Icon(Icons.edit_sharp), onPressed: () {
                                            onEditClick(
                                                infos, username);
                                          },)
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                40, 5, 40, 10),
                                            child: IconButton(icon: Icon(Icons.delete),
                                              onPressed: () {
                                              _showDialog();
                                                  },
                                            )
                                          ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            )
                          : SizedBox(),
                      onClickDothi
                          ? Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0.0, 5.0, 0.0, 5.0),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: const EdgeInsets.all(2.0),
                                          child: Container(
                                              decoration: new BoxDecoration(
                                                  borderRadius:
                                                      new BorderRadius.only(
                                                          topLeft: const Radius
                                                              .circular(5.0),
                                                          topRight: const Radius
                                                              .circular(5.0))),
                                              child: Column(
                                                children: [
                                                  Container(
                                                    // width: 40,
                                                    child: Center(
                                                      child: new Text(
                                                        "Từ:",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color: Colors.blue,
                                                            fontSize: 15),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              )),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Padding(
                                          padding: const EdgeInsets.all(2.0),
                                          child: Container(
                                              height: 60,
                                              decoration: new BoxDecoration(
                                                  // color: Colors.blue,
                                                  borderRadius:
                                                      new BorderRadius.only(
                                                          topLeft: const Radius
                                                              .circular(5.0),
                                                          topRight: const Radius
                                                              .circular(5.0))),
                                              child: new Center(
                                                child: FlatButton(
                                                    onPressed: () async {
                                                      if(await isInternet() == false) return;
                                                      _showDate().then((date) {
                                                        if (date == null) {
                                                          date = DateTime.now();
                                                        }

                                                        //print('ddthanh${_dateFrom.toString()}');
                                                        setState(() {
                                                          _dateFrom = date;
                                                          addItemToList(_selection);
                                                        });
                                                      });
                                                    },
                                                    child: Text(_dateFrom ==
                                                            null
                                                        ? "Chọn ngày"
                                                        : _dateFrom.toString().substring(0, 10).split("-")[2]+"/"+_dateFrom.toString().substring(0, 10).split("-")[1]+"/"+_dateFrom.toString().substring(0, 10).split("-")[0])),
                                              )),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: const EdgeInsets.all(2.0),
                                          child: Container(
                                              decoration: new BoxDecoration(
                                                  borderRadius:
                                                      new BorderRadius.only(
                                                          topLeft: const Radius
                                                              .circular(5.0),
                                                          topRight: const Radius
                                                              .circular(5.0))),
                                              child: Column(
                                                children: [
                                                  Container(
                                                    // width: 40,
                                                    child: Center(
                                                      child: new Text(
                                                        "Đến:",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color: Colors.blue,
                                                            fontSize: 15),
                                                      ),
                                                    ),

                                                  ),
                                                ],
                                              )),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Padding(
                                          padding: const EdgeInsets.all(2.0),
                                          child: Container(
                                              height: 60,
                                              decoration: new BoxDecoration(
                                                  // color: Colors.blue,
                                                  borderRadius:
                                                      new BorderRadius.only(
                                                          topLeft: const Radius
                                                              .circular(5.0),
                                                          topRight: const Radius
                                                              .circular(5.0))),
                                              child: new Center(
                                                child: FlatButton(
                                                    onPressed: () async{
                                                      if(await isInternet() == false) return;
                                                      _showDate().then((date) {
                                                        if (date == null) {
                                                          date = DateTime.now();
                                                          //print('ddthanh${date.toString()}');
                                                        }

                                                        //print('ddthanh${_dateTo.toString()}');
                                                        setState(() {
                                                          _dateTo = date;
                                                          addItemToList(_selection);

                                                        });
                                                      });
                                                    },
                                                    child: Text(
                                                         _dateTo == null ? "Chọn ngày" :
                                                    _dateTo.toString().substring(0, 10).split("-")[2]+"/"+_dateTo.toString().substring(0, 10).split("-")[1]+"/"+_dateTo.toString().substring(0, 10).split("-")[0]),
                                              )),
                                        ),
                                      )),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      5.0, 5.0, 5.0, 5.0),
                                  child: Container(
                                    height: 3,
                                    color: Colors.black,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    child: Text(
                                      textChart,
                                      style: TextStyle(fontSize: 18),
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 450,
                                  color: Colors.white,
                                  child: SfCartesianChart(
                                      // title: ChartTitle(
                                      //     text: _selection == null
                                      //         ? "P_out"
                                      //         : _selection),
                                      zoomPanBehavior: ZoomPanBehavior(
                                          // Enables pinch zooming
                                          enablePinching: true),
                                      tooltipBehavior:
                                          TooltipBehavior(enable: true),
                                      legend: Legend(isVisible: false),
                                      primaryXAxis: CategoryAxis(),
                                      series: <ChartSeries<ChartData, String>>[
                                        StackedLineSeries<ChartData, String>(
                                          enableTooltip: true,
                                          dataSource: chartData,
                                          xValueMapper: (ChartData sales, _) =>
                                              sales.x,
                                          yValueMapper: (ChartData sales, _) =>
                                              sales.y,
                                          name: _selection == null
                                              ? "P_out(W)"
                                              : _selection,
                                          // same y variable used but assign y data value from series -1 data source.
                                        ),
                                      ]),
                                ),

                              ],
                            )
                          : SizedBox(),
                      onClickSanluong
                          ? Column(
                              //mainAxisAlignment: MainAxisAlignment.center,
                              //crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                  child: Container(
                                    // height: 90,
                                    // width: 270,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black,
                                      ),
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Expanded(
                                              flex: 1,
                                              child: new Column(
                                                children: <Widget>[
                                                  new Text(
                                                    'TTGHĐ',
                                                    style: (_radioValue==1) ? TextStyle(fontSize: 14.0, color:Colors.red) : TextStyle(fontSize: 14.0, color:Colors.black),
                                                    // style: new TextStyle(fontSize: 14.0,),
                                                  ),
                                                  new Radio(
                                                    value: 1,
                                                    activeColor: Colors.red,
                                                    groupValue: _radioValue,
                                                    onChanged:
                                                    !(show_btn ) ? null : _handleRadioValueChange,
                                                  ),
                                                ],
                                              )),
                                          Expanded(
                                              flex: 1,
                                              child: new Column(
                                                children: <Widget>[
                                                  new Text(
                                                    'P max',
                                                    style: (_radioValue==4) ? TextStyle(fontSize: 14.0, color:Colors.red) : TextStyle(fontSize: 14.0, color:Colors.black),
                                                    // style: new TextStyle(fontSize: 14.0,),
                                                  ),
                                                  new Radio(
                                                    value: 4,
                                                    activeColor: Colors.red,
                                                    groupValue: _radioValue,
                                                    onChanged:
                                                    !(show_btn ) ? null : _handleRadioValueChange,
                                                  ),
                                                ],
                                              )),
                                          Expanded(
                                              flex: 1,
                                              child: new Column(
                                                children: <Widget>[
                                                  new Text(
                                                    'SL Ngày',
                                                    style: (_radioValue==0) ? TextStyle(fontSize: 14.0, color:Colors.red) : TextStyle(fontSize: 14.0, color:Colors.black),
                                                    // style: new TextStyle(fontSize: 14.0),
                                                  ),
                                                  new Radio(
                                                    value: 0,
                                                    activeColor: Colors.red,
                                                    groupValue: _radioValue,
                                                    onChanged:
                                                    !(show_btn ) ? null : _handleRadioValueChange,
                                                  ),
                                                ],
                                              )),

                                          Expanded(
                                              flex: 1,
                                              child: new Column(
                                                children: <Widget>[
                                                  new Text(
                                                    'SLLK',
                                                    style: (_radioValue==2) ? TextStyle(fontSize: 14.0, color:Colors.red) : TextStyle(fontSize: 14.0, color:Colors.black),
                                                    //style: new TextStyle(fontSize: 14.0),
                                                  ),
                                                  new Radio(
                                                    value: 2,
                                                    activeColor: Colors.red,
                                                    groupValue: _radioValue,
                                                    onChanged:
                                                    !(show_btn ) ? null : _handleRadioValueChange,
                                                  ),
                                                ],
                                              )),
                                          Expanded(
                                              flex: 1,
                                              child: new Column(
                                                children: <Widget>[
                                                  new Text(
                                                    'Sản lượng',
                                                  style: (_radioValue==3) ? TextStyle(fontSize: 14.0, color:Colors.red) : TextStyle(fontSize: 14.0, color:Colors.black),
                                                  ),
                                                  new Radio(
                                                    value: 3,
                                                    activeColor: Colors.red,
                                                    groupValue: _radioValue,
                                                    onChanged:
                                                    !(show_btn ) ? null :_handleRadioValueChange,
                                                  ),
                                                ],
                                              )),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                !is_sanluong
                                    ? Padding(
                                        padding: const EdgeInsets.all(10.0))
                                    : Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Container(
                                          // height: 64,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: Colors.black,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 1,
                                                  child: new Column(
                                                    children: <Widget>[
                                                      new Text(
                                                        'Tháng',
                                                        style: (_radio1==0) ? TextStyle(fontSize: 14.0, color:Colors.red) : TextStyle(fontSize: 14.0, color:Colors.black),
                                                        // style: new TextStyle(fontSize: 14.0),
                                                      ),
                                                      new Radio(
                                                        value: 0,
                                                        activeColor: Colors.red,
                                                        groupValue: _radio1,
                                                        onChanged:
                                                        !(show_btn ) ? null : _handleRadioValueChange1,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 1,
                                                  child: new Column(
                                                    children: <Widget>[
                                                      new Text(
                                                        'Năm',
                                                        style: (_radio1==1) ? TextStyle(fontSize: 14.0, color:Colors.red) : TextStyle(fontSize: 14.0, color:Colors.black),
                                                        // style: new TextStyle(fontSize: 14.0),
                                                      ),
                                                      new Radio(
                                                        value: 1,
                                                        activeColor: Colors.red,
                                                        groupValue: _radio1,
                                                        onChanged:
                                                        !(show_btn ) ? null : _handleRadioValueChange1,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 1,
                                                  child: new Column(
                                                    children: <Widget>[
                                                      new Text(
                                                        'Tùy chọn',
                                                        style: (_radio1==2) ? TextStyle(fontSize: 14.0, color:Colors.red) : TextStyle(fontSize: 14.0, color:Colors.black),
                                                        // style: new TextStyle(fontSize: 14.0),
                                                      ),
                                                      new Radio(
                                                        value: 2,
                                                        activeColor: Colors.red,
                                                        groupValue: _radio1,
                                                        onChanged:
                                                        !(show_btn ) ? null : _handleRadioValueChange1,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                Center(
                                  child: !is_sanluong
                                      ? Padding(
                                          padding: const EdgeInsets.all(2.0),
                                          child: Container(
                                              height: 40,
                                              decoration: new BoxDecoration(
                                                  // color: Colors.blue,
                                                  borderRadius:
                                                      new BorderRadius.only(
                                                          topLeft: const Radius
                                                              .circular(5.0),
                                                          topRight: const Radius
                                                              .circular(5.0))),
                                              child: new Center(
                                                child: FlatButton(
                                                    onPressed: () async {
                                                      if(await isInternet() == false) return;
                                                      _showDate().then((date) {
                                                        if (date == null) {
                                                          date = DateTime.now();
                                                        }

                                                        //EasyLoading.showToast(date.toString());
                                                        setState(() {
                                                          _dateFrom = date;
                                                          addSanLuong(
                                                              _selection,
                                                              _radioValue.toString(),
                                                              _radio1.toString());
                                                        });
                                                      });
                                                    },
                                                    child: Text(
                                                      _dateFrom == null
                                                          ? DateTime.now().toString().substring(0, 10).split("-")[2]+"/"+DateTime.now().toString().substring(0, 10).split("-")[1]+"/"+DateTime.now().toString().substring(0, 10).split("-")[0] // "Lựa chọn thời gian"
                                                          : ( _dateFrom.toString().substring(0, 10).split("-")[2]+"/"+_dateFrom.toString().substring(0, 10).split("-")[1]+"/"+_dateFrom.toString().substring(0, 10).split("-")[0]),
                                                      style: TextStyle(
                                                          fontSize: 18,
                                                          color: Colors.black),
                                                    )),
                                              )),
                                        )
                                      : Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0.0, 5.0, 0.0, 5.0),
                                          child: Column(
                                            children: [
                                              Container(
                                                child: (_radio1 < 2)
                                                    ? Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      flex: 2,
                                                      child: Padding(
                                                        padding:
                                                        const EdgeInsets
                                                            .all(2.0),
                                                        child: Container(
                                                            height: 60,
                                                            decoration:
                                                            new BoxDecoration(
                                                              // color: Colors.blue,
                                                                borderRadius: new BorderRadius
                                                                    .only(
                                                                    topLeft: const Radius.circular(
                                                                        5.0),
                                                                    topRight:
                                                                    const Radius.circular(5.0))),
                                                            child: new Center(
                                                              child:
                                                              FlatButton(
                                                                onPressed: () async {
                                                                  if(await isInternet() == false) return;
                                                                  _showDate()
                                                                      .then((date) {
                                                                    if (date == null) {
                                                                      date = DateTime.now();
                                                                    }

                                                                    //print('ddthanh${_dateFrom.toString()}');
                                                                    setState(() {
                                                                      _dateFrom = date;
                                                                      addSanLuong(
                                                                          _selection,
                                                                          _radioValue.toString(),
                                                                          _radio1.toString());
                                                                    });
                                                                  });
                                                                },
                                                                child:
                                                                (_radio1 ==
                                                                    0)
                                                                    ? Text(
                                                                  _dateFrom == null ? "Chọn ngày" : "Tháng " + _dateFrom.toString().substring(5, 7) + "/" + _dateFrom.toString().substring(0,4),
                                                                  style: TextStyle(fontSize: 20),
                                                                )
                                                                    : Text(
                                                                  _dateFrom == null ? "Chọn ngày" : "Năm " + _dateFrom.toString().substring(0, 4),
                                                                  style: TextStyle(fontSize: 20),
                                                                ),
                                                              ),
                                                            )),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                                    : Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      flex: 1,
                                                      child: Padding(
                                                        padding:
                                                        const EdgeInsets
                                                            .all(2.0),
                                                        child: Container(
                                                            decoration: new BoxDecoration(
                                                                borderRadius: new BorderRadius
                                                                    .only(
                                                                    topLeft:
                                                                    const Radius.circular(
                                                                        5.0),
                                                                    topRight:
                                                                    const Radius.circular(
                                                                        5.0))),
                                                            child: Column(
                                                              children: [
                                                                Container(
                                                                  // width: 40,
                                                                  child:
                                                                  Center(
                                                                    child:
                                                                    new Text(
                                                                      "Từ:",
                                                                      textAlign:
                                                                      TextAlign.center,
                                                                      style: TextStyle(
                                                                          color:
                                                                          Colors.blue,
                                                                          fontSize: 15),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            )),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 3,
                                                      child: Padding(
                                                        padding:
                                                        const EdgeInsets
                                                            .all(2.0),
                                                        child: Container(
                                                            height: 60,
                                                            decoration:
                                                            new BoxDecoration(
                                                              // color: Colors.blue,
                                                                borderRadius: new BorderRadius
                                                                    .only(
                                                                    topLeft: const Radius.circular(
                                                                        5.0),
                                                                    topRight:
                                                                    const Radius.circular(5.0))),
                                                            child: new Center(
                                                              child:
                                                              FlatButton(
                                                                  onPressed: () async {
                                                                    if(await isInternet() == false) return;
                                                                    _showDate()
                                                                        .then((date) {
                                                                      if (date == null) {
                                                                        date = DateTime.now();
                                                                      }
                                                                      _dateFrom = date;
                                                                      //print('ddthanh${_dateFrom.toString()}');
                                                                      setState(() {
                                                                        _dateFrom = date;
                                                                        addSanLuong(
                                                                            _selection,
                                                                            _radioValue.toString(),
                                                                            _radio1.toString());
                                                                      });
                                                                    });
                                                                  },
                                                                  child:
                                                                  Text(
                                                                    _dateFrom == null
                                                                        ? "Chọn ngày"
                                                                        : _dateFrom.toString().substring(0, 10).split("-")[2]+"/"+_dateFrom.toString().substring(0, 10).split("-")[1]+"/"+_dateFrom.toString().substring(0, 10).split("-")[0],
                                                                    style:
                                                                    TextStyle(fontSize: 15),
                                                                  )),
                                                            )),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 1,
                                                      child: Padding(
                                                        padding:
                                                        const EdgeInsets
                                                            .all(2.0),
                                                        child: Container(
                                                            decoration: new BoxDecoration(
                                                                borderRadius: new BorderRadius
                                                                    .only(
                                                                    topLeft:
                                                                    const Radius.circular(
                                                                        5.0),
                                                                    topRight:
                                                                    const Radius.circular(
                                                                        5.0))),
                                                            child: Column(
                                                              children: [
                                                                Container(
                                                                  // width: 40,
                                                                  child:
                                                                  Center(
                                                                    child:
                                                                    new Text(
                                                                      "Đến:",
                                                                      textAlign:
                                                                      TextAlign.center,
                                                                      style: TextStyle(
                                                                          color:
                                                                          Colors.blue,
                                                                          fontSize: 15),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            )),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 3,
                                                      child: Padding(
                                                        padding:
                                                        const EdgeInsets
                                                            .all(2.0),
                                                        child: Container(
                                                            height: 60,
                                                            decoration:
                                                            new BoxDecoration(
                                                              // color: Colors.blue,
                                                                borderRadius: new BorderRadius
                                                                    .only(
                                                                    topLeft: const Radius.circular(
                                                                        5.0),
                                                                    topRight:
                                                                    const Radius.circular(5.0))),
                                                            child: new Center(
                                                              child:
                                                              FlatButton(
                                                                  onPressed: () async{
                                                                    if(await isInternet() == false) return;
                                                                    _showDate()
                                                                        .then((date) {
                                                                      if (date == null) {
                                                                        date = DateTime.now();
                                                                      }
                                                                      _dateTo = date;
                                                                      setState(() {
                                                                        _dateTo = date;
                                                                        addSanLuong(
                                                                            _selection,
                                                                            _radioValue.toString(),
                                                                            _radio1.toString());
                                                                      });
                                                                    });
                                                                  },
                                                                  child:
                                                                  Text(
                                                                    _dateTo == null
                                                                        ? "Chọn ngày"
                                                                        : _dateTo.toString().substring(0, 10).split("-")[2]+"/"+_dateTo.toString().substring(0, 10).split("-")[1]+"/"+_dateTo.toString().substring(0, 10).split("-")[0],
                                                                    style:
                                                                    TextStyle(fontSize: 15),
                                                                  )),
                                                            )),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                child: Text("Tổng sản lượng: "+value_tongsanluong.toStringAsFixed(2)+ " kWh",
                                                  style: TextStyle(fontSize: 15, color: Colors.redAccent),)
                                              )
                                            ],
                                          ),

                                        ),
                                ),

                                Container(
                                  height: 320,
                                  width: double.infinity,
                                  padding: const EdgeInsets.all(12),
                                  child: new charts.BarChart(
                                    seriesList,
                                    behaviors: [
                                      LinePointHighlighter(
                                          symbolRenderer:
                                              CustomCircleSymbolRenderer())
                                    ],
                                    selectionModels: [
                                      new charts.SelectionModelConfig(
                                        changedListener:
                                            (SelectionModel model) {
                                          setState(() {
                                            textSelected = (model
                                                    .selectedSeries[0]
                                                    .measureFn(model
                                                        .selectedDatum[0]
                                                        .index))
                                                .toString();
                                            AppStaticVar.valueDisplay =
                                                textSelected;
                                          });

                                          // print( model.selectedSeries[0].measureFn(model.selectedDatum[0].index));
                                        },
                                      )
                                    ],
                                    animate: animate,
                                    // defaultRenderer:
                                    // new charts.BarRendererConfig(
                                    //   groupingType:
                                    //   charts.BarGroupingType.grouped, barRendererDecorator: new charts.BarLabelDecorator(outsideLabelStyleSpec: new charts.TextStyleSpec(fontSize: 10))
                                    // ),
                                  ),

                                ),
                               Center(
                                 child: Text(_radioValue==1?"(giờ)": (_radioValue==4)?"(W)":"(kWh)", style: TextStyle(fontSize: 18, color: Colors.black),)
                               ),
                              ],
                            )
                          : SizedBox(),
                    ],
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SafeArea(
                    child: Container(
                      color: Colors.black12,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Container(
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(5.0),
                                        topRight: const Radius.circular(5.0))),
                                child: Center(
                                  child: FlatButton(
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0, 10, 0, 0),
                                          child: Center(
                                              child: Image.asset(
                                                  'assets/user2.png')),
                                        ),
                                        Text(
                                          'Thông tin',
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: onClickCauHinh
                                                  ? Colors.blue
                                                  : null),
                                        ),
                                      ],
                                    ),
                                    onPressed:
                                        !(show_btn ) ? null : () => onCauhinhClick(),
                                    // onPressed: () {
                                    //   setState(() {
                                    //     onClickCauHinh = true;
                                    //     onClickDothi = false;
                                    //     onClickSanluong = false;
                                    //   });
                                    // },
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Container(
                                decoration: new BoxDecoration(
                                    // color: Colors.blue,
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(5.0),
                                        topRight: const Radius.circular(5.0))),
                                child: Center(
                                  child: FlatButton(
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0, 10, 0, 0),
                                          child: Center(
                                              child: Image.asset(
                                                  'assets/line-chart.png')),
                                        ),
                                        Text(
                                          'Đồ thị',
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: onClickDothi
                                                  ? Colors.blue
                                                  : null),
                                        ),
                                      ],
                                    ),
                                    onPressed: !show_btn
                                        ? null
                                        : () => addItemToList(_selection),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Container(
                                  decoration: new BoxDecoration(
                                      borderRadius: new BorderRadius.only(
                                          topLeft: const Radius.circular(5.0),
                                          topRight: const Radius.circular(5.0))),
                                  child: Center(
                                    child: FlatButton(
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                0, 10, 0, 0),
                                            child: Center(
                                                child: Image.asset(
                                                    'assets/graphic.png')),
                                          ),
                                          Text(
                                            'Thống kê',
                                            style: TextStyle(
                                                color: onClickSanluong
                                                    ? Colors.blue
                                                    : null),
                                          ),
                                        ],
                                      ),
                                      onPressed: !(show_btn)
                                          ? null
                                          : () => addSanLuong(
                                              _selection,
                                              _radioValue.toString(),
                                              _radio1.toString()),
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      // drawer: _createDrawer(context), menu
    );
  }

  Future<DateTime> _showDate() async {
    DateTime selectedDate = DateTime.now();
    // String formattedDate = DateFormat('kk:mm:ss \n EEE d MMM').format(now);

    DateTime date = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2018),
        lastDate: DateTime(2030),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light(),
            child: child,
          );
        });
    return date;
  }

  //on click Thong ke
  void addSanLuong(String selection, String choice, String radio) async {
    // print('choice :${choice}');
    // print('sel :${selection}');
    //ViewNgay(choice); // EasyLoading.showToast(selection);

    var arr_imei = [];
    setState(() {
      onClickDothi = false;
      onClickCauHinh = false;
      onClickSanluong = true;
    });

      // _dateFrom = DateTime.now();
      if (choice == "3") {
        switch (radio) {
          case "0": //thang
            // EasyLoading.showToast("Thang");

            ViewThang(radio);
            break;
          case "1": //nam
            ViewNam(radio);

            break;

          case "2": //tuy chon
            ViewTuychon(radio);
            break;
        }
      } else {
        ViewNgay(choice);
      }
  }

  //on click Do thi
  void addItemToList(String selection) async {
    var start;
    var end;

    setState(() {
      if (selection == null) {
        selection = "P_out(W)";
      }
      textChart = selection;
      onClickDothi = true;
      onClickCauHinh = false;
      onClickSanluong = false;
    });
    if(await isInternet() == false) return;
    chartData.clear();

    if (_dateFrom == null || _dateTo == null) {
      EasyLoading.showToast("Lựa chọn thời gian");

      return;
    }
    start = _dateFrom.toString().substring(0, 10);
    end = _dateTo.toString().substring(0, 10);
    var start_year = _dateFrom.toString().substring(0, 4);
    var end_year = _dateTo.toString().substring(0, 4);
    var start_dd = _dateFrom.toString().substring(8, 10);
    var start_mm = _dateFrom.toString().substring(5, 7);
    var start_yy = _dateFrom.toString().substring(2, 4);
    var end_dd = _dateTo.toString().substring(8, 10);
    var end_mm = _dateTo.toString().substring(5, 7);
    var end_yy = _dateTo.toString().substring(2, 4);

    var leng_ngay = int.parse(end_dd) - int.parse(start_dd);
    var leng_thang = int.parse(end_mm) - int.parse(start_mm);
    var leng_nam = int.parse(end_yy) - int.parse(start_yy);
    if ((leng_nam < 0) ||
        (leng_nam == 0 && leng_thang < 0) ||
        (leng_thang == 0 && leng_ngay < 0)) {
      //thang sau < thang trc
      EasyLoading.showToast("Khoảng thời gian không hợp lệ");
      return;
    } else {
      var stringStart = start_year + "-" + start_mm + "-" + start_dd;
      var stringEnd = end_year + "-" + end_mm + "-" + end_dd;

      final start123 = DateTime.tryParse(stringStart);
      final end123 = DateTime.tryParse(stringEnd);
      //final difference = end123.difference(start123).inDays;
      //var leng_view = difference+1;
      var arrDays = getDaysInBeteween(start123, end123);
      //print('leng_view :${arrDays.length}');
      //print('leng_view :${arrDays}');

      var arr_imei = [];
      // double sum = 0;
      for (int i = 0; i < arrDays.length; i++) {
        var day1 = arrDays[i].toString().substring(8, 10);
        var month1 = arrDays[i].toString().substring(5, 7);
        var year1 = arrDays[i].toString().substring(2, 4);
        var elementId =
            imei + "_" + day1.padLeft(2, '0') + month1.padLeft(2, '0') + year1;
        arr_imei.add(elementId);
      }
      // var start_imei = imei + "_" + start_dd + start_mm + start_yy;
      // var end_imei = imei + "_" + end_dd + end_mm + end_yy;
      var cnt_null = 0;
      for (int i = 0; i < arr_imei.length; i++) {
        show_btn = false;

        //print('data of imei i: ${arr_imei[i]}');
        await bloc.viewChart(arr_imei[i]).then((error) {
          setState(() {
            if (error != null) {
              cnt_null++;
              if (cnt_null == arr_imei.length) {
                chartData.clear();
                EasyLoading.showToast("Không có dữ liệu");
              }
              //EasyLoading.showToast(error);
              return;
            } else {
              value = bloc.dataChart;
              // if (selection == null) {
              //   selection = "P_out";
              // }
              switch (selection) {
                case "E_today(kW)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time, //.substring(0, 14)
                          double.parse(value[0].valueMesument[i].eToday)));
                    }
                  }
                  break;

                case "P_in(W)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].pIn)));
                    }
                  }
                  break;
                case "P_out(W)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].pOut)));
                    }
                  }
                  break;
                case "P_max(W)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].pMaxtoday)));
                    }
                  }
                  break;

                case "V_grid(V)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].vGrid)));
                    }
                  }
                  break;
                case "I_grid(A)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].iGrid)));
                    }
                  }
                  break;
                case "F_grid(Hz)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].fGrid)));
                    }
                  }
                  break;

                case "PF":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].powerFactor)));
                    }
                  }
                  break;
                case "V_PV1(V)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].vPv1)));
                    }
                  }
                  break;
                case "I_PV1(A)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].iPv1)));
                    }
                  }
                  break;

                case "V_PV2(V)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].vPv2)));
                    }
                  }
                  break;
                case "I_PV2(A)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].iPv2)));
                    }
                  }
                  break;

                //////
                case "I_leakage(mA)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].iLeakage)));
                    }
                  }
                  break;

                case "Temp(oC)":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].temp)));
                    }
                  }
                  break;
                case "SLLK":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].sllk)));
                    }
                  }
                  break;
                case "TTGHD":
                  {
                    for (int i = 0; i < value[0].valueMesument.length; i++) {
                      //print('data: ${value[0].valueMesument[i].pOut}');
                      // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
                      chartData.add(new ChartData(
                          value[0].valueMesument[i].time,
                          double.parse(value[0].valueMesument[i].ttghd)));
                    }
                  }
                  break;

                default:
                  {
                    //EasyLoading.showToast("Lua chon");
                  }
                  break;
              }

              // }); //
            }

          }); //
        });
      }
      show_btn = true;
    }
  }

  void onEditClick(List<ViewInfo> infos, String username) async{
    //EasyLoading.showToast(imei);
    if(await isInternet() == false) return;
    Navigator.push(
        context,
        MaterialPageRoute(
        ));
  }

  void onCauhinhClick() {
    setState(() {
      onClickDothi = false;
      onClickCauHinh = true;
      onClickSanluong = false;
    });
  }

  void ViewNgay(String selection) async {
    // if (_dateFrom == null) {
    //   return;
    // }
    var date = _dateFrom.toString();

    var elementId = imei +
        "_" +
        date.substring(8, 10).padLeft(2, '0') +
        date.substring(5, 7).padLeft(2, '0') +
        date.substring(2, 4);

    if(await isInternet() == false) return;
    setState(() {
      viewNgayClicked = true;
      viewThangClicked = false;
      viewNamClicked = false;
      viewTuychonClicked = false;
      desktopSalesData.clear();
      seriesList = _createRadomData();
    });


    await bloc.viewChart(elementId).then((error) {
      setState(() {
        // viewNgayClicked = true;
        // viewThangClicked = false;
        // viewNamClicked = false;
        // viewTuychonClicked = false;
        // desktopSalesData.clear();
        seriesList = _createRadomData();
        if (error != null) {
          EasyLoading.showToast("Không có dữ liệu");
          //clear Chart

          return;
        } else {
          // EasyLoading.showToast("hello");
          value = bloc.dataChart;
          // EasyLoading.showToast(value.length.toString());
          var counter = value[0].valueMesument.length - 1;
          var time = value[0].valueMesument[counter].time.substring(0, 8);
          switch (selection) {
            case "0":
              {
                //print("E+today");
                // EasyLoading.showToast("etoday");
                //EasyLoading.showToast(value[0].valueMesument[counter].eToday);
                var etoday = value[0].valueMesument[counter].eToday;
                // print('elementId: ${elementId}');
                DataShow(time, double.parse(etoday));
                desktopSalesData.add(DataShow(time, double.parse(etoday)));
              }
              break;
            case "1":
              {
                // print("E+today");

                //EasyLoading.showToast(value[0].valueMesument[counter].ttghd);
                var etoday = value[0].valueMesument[counter].ttghd;
                // print('elementId: ${elementId}');
                DataShow(time, double.parse(etoday));
                desktopSalesData.add(DataShow(time, double.parse(etoday)));
              }
              break;
            case "2":
              {
                // print("E+today");

                //EasyLoading.showToast(value[0].valueMesument[counter].sllk);
                var etoday = value[0].valueMesument[counter].sllk;
                // print('elementId: ${elementId}');
                DataShow(time, double.parse(etoday));
                desktopSalesData.add(DataShow(time, double.parse(etoday)));
              }
              break;
            case "4": //P_max
              {
                // EasyLoading.showToast("SL");
                var p_max = value[0].valueMesument[counter].pMaxtoday;
                DataShow(time, double.parse(p_max));
                desktopSalesData.add(DataShow(time, double.parse(p_max)));
              }
              break;
          }
        }
      });
    });
  }

  void ViewThang(String selection) async {
    // is_test = false;
    // EasyLoading.showToast(selection);
    if (_dateFrom == null) {
      EasyLoading.showToast("Lựa chọn thời gian");
      return;
    }
    var number_thang = _dateFrom.toString().substring(5, 7);
    //print('thang: ${number_thang}');
    var year = _dateFrom.toString().substring(2, 4);
    //print('nam: ${year}');
    var val_selection = selection;
    //thang A => so ngay trong thang
    var number_date = daysInMonth(_dateFrom);
    //EasyLoading.showToast(number_date.toString());
    var arr_imei = [];
    for (int i = 1; i <= number_date; i++) {
      var elementId = imei +
          "_" +
          i.toString().padLeft(2, '0') +
          number_thang.padLeft(2, '0') +
          year;
      arr_imei.add(elementId);
    }
    //print('data of imei i: ${arr_imei}');
    //EasyLoading.showToast(number_thang.toString() + " + " + val_selection);

    //has imei = > get data
    var cnt_null = 0;
    value_tongsanluong =0;
    setState(() {
      viewNgayClicked = false;
      viewThangClicked = true;
      viewNamClicked = false;
      viewTuychonClicked = false;
      desktopSalesData.clear();
      seriesList = _createRadomData();
    });

    // var arr = [];
    for (int i = 0; i < arr_imei.length; i++) {
      show_btn = false;
      //print('data of imei i: ${arr_imei[i]}');
      await bloc.viewChart(arr_imei[i]).then((error) {
        setState(() {
          if (error != null) {
            //print('${arr_imei[i]} ko co data');
            cnt_null++;
            if (cnt_null == arr_imei.length) {
              chartData.clear();
              EasyLoading.showToast("Không có dữ liệu");
            }
            //EasyLoading.showToast(error);
            return;
          } else {
            value = bloc.dataChart;
            if (value == null) {
              //print('${arr_imei[i]} ko co data');
            }

            //print("E+today");

            var leng_dataInDay = value[0].valueMesument.length;
            var x =
                value[0].valueMesument[leng_dataInDay - 1].time.substring(0, 2);
            var y =
                double.parse(value[0].valueMesument[leng_dataInDay - 1].eToday);
            //print('data x: ${x.toString().substring(0, 2)}');
            value_tongsanluong = value_tongsanluong + y;
            // print('data y: ${y.toString()}');
            desktopSalesData.add(DataShow(x, y));

            //add data to Chart

            // }); //
          }

          // print('data y= : ${chartData.length}');
          // for (int i = 0; i < chartData.length; i++) {
          //   // print('data y= : ${chartData[i].x}');
          //   print('data y= : ${chartData[i].y}');
          //   sum = sum + chartData[i].y;
          // }
          // print('data sum= : ${sum}');
          // meanValue = sum / (chartData.length);
          // desktopSalesData.add(arr);
          // _createRadomData();
          seriesList = _createRadomData();
        }); //
      });
    }
    // print('data sum= : ${value_tongsanluong}');
    // });
    show_btn = true;
  }

  //
  void ViewNam(String selection) async {
    // is_test = false;
    // EasyLoading.showToast(selection);
    if (_dateFrom == null) {

      EasyLoading.showToast("Lựa chọn thời gian");
      return;
    }
    //var number_thang = _dateFrom.toString().substring(5,7);
    //print('thang: ${number_thang}');
    var year = _dateFrom.toString().substring(2, 4);
    //print('nam: ${year}');
    var years = _dateFrom.toString().substring(0, 4);
    //print('nam full: ${years}');
    var arr_imei = [];
    for (int i = 1; i <= 12; i++) {
      var elementId = imei + "_" + i.toString().padLeft(2, '0') + year;
      arr_imei.add(elementId);
    }
    //print('arr: ${arr_imei}');

    //imei => send imei 2021 => receive data month,year => array get

    GetSmonth dataSmonth;

    setState(() {
      viewNgayClicked = false;
      viewThangClicked = false;
      viewNamClicked = true;
      viewTuychonClicked = false;
      desktopSalesData.clear();
      seriesList = _createRadomData();
    });
    var cnt_null = 0;
    value_tongsanluong = 0;
    for (int i = 1; i <= 12; i++) {
      show_btn = false;
      var mmyy = i.toString().padLeft(2, '0') + year;
      await bloc.getDays(imei, mmyy).then((error) {
        // EasyLoading.showToast("getting ..");
        dataSmonth = bloc.dataSmonth;
        if (dataSmonth == null) {
          cnt_null++;
          if (cnt_null == 12) {
            EasyLoading.showToast("Không có dữ liệu");
          }
          return;
        }

        // for (int i = 0; i < dataSmonth.length; i++) {

        setState(() {
          // if (value == null) {
          //   print('${arr_imei[i]} ko co data');
          // }
          //("E+today");
          var x = dataSmonth.month;
          // print('data y: ${x.toString()}');
          var y = dataSmonth.sumEtoday == null
              ? null
              : double.parse(dataSmonth.sumEtoday.padRight(2, "0"));
          //print('data x: ${x.toString()}');
          //print('data y: ${y.toString()}');
          if(y != null){
            value_tongsanluong = value_tongsanluong + y;
          }
          desktopSalesData.add(DataShow(x, y));

          seriesList = _createRadomData();
        });

        return;
      });
    }
    setState(() {
      show_btn = true;
    });
    //print
    // print('data sum= : ${value_tongsanluong}');
    return;
  }

  Future<String> deleteDevice(String username, String link) async {
    //print('admin :${username}');
    //print('pass:${password}');
    return bloc1.delete(username, widget.imei);
    // return bloc1.delete(username, imei).then((error) {
    //   bloc2.login(username, password,link).then((error) {
    //     Navigator.push(
    //         context,
    //         MaterialPageRoute(
    //           builder: (context) => ViewDevice(
    //               username: bloc2.user.email, password: bloc2.user.password),
    //         ));
    //   });
    // });
  }

  //delete pop up
  void _showDialog() async{
    if(await isInternet() == false) return;
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Xóa thiết bị",style: TextStyle(color: Colors.red),),
          content: new Text("Bạn muốn xóa thiết bị?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Hủy"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Đồng ý"),
              onPressed: () {
                Navigator.pop(ctx);
                deleteDevice(username, imei).then((value) {
                  // print("ddthanh $value");
                  Navigator.pop(context, "delete device");
                });
              },
            ),
          ],
        );
      },
    );
  }

  void _showDialog2(String errcode) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Mã lỗi " +errcode,style: TextStyle(color: Colors.red),),
          content:
          Container(
            height: 300,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Mô tả:", style: TextStyle(fontWeight: FontWeight.bold),),
                Text(mean[int.parse(errcode)]),
                Text("Hướng xử lý:", style: TextStyle(fontWeight: FontWeight.bold)),
                Text(fix[int.parse(errcode)]),
              ],
            ),
          ),

          //new Text("Bạn muốn xóa thiết bị?"),// functioncheck err
          // actions: <Widget>[
          //   // usually buttons at the bottom of the dialog
          //   new FlatButton(
          //     child: new Text("Hủy"),
          //     onPressed: () {
          //       Navigator.of(context).pop();
          //     },
          //   ),
          //   new FlatButton(
          //     child: new Text("Đồng ý"),
          //     onPressed: () {
          //       Navigator.pop(ctx);
          //       deleteDevice(username, imei).then((value) {
          //         // print("ddthanh $value");
          //         Navigator.pop(context, "delete device");
          //       });
          //     },
          //   ),
          // ],
        );
      },
    );
  }


  void ViewTuychon(String selection) async {
    // is_test = true;
    if (_dateFrom == null || _dateTo == null) {
      EasyLoading.showToast("Lựa chọn thời gian");

      return;
    }
    //EasyLoading.showToast(selection);
    var start = _dateFrom.toString().substring(0, 10);
    var end = _dateTo.toString().substring(0, 10);
    var start_year = _dateFrom.toString().substring(0, 4);
    var end_year = _dateTo.toString().substring(0, 4);
    var start_dd = _dateFrom.toString().substring(8, 10);
    var start_mm = _dateFrom.toString().substring(5, 7);
    var start_yy = _dateFrom.toString().substring(2, 4);
    var end_dd = _dateTo.toString().substring(8, 10);
    var end_mm = _dateTo.toString().substring(5, 7);
    var end_yy = _dateTo.toString().substring(2, 4);

    var leng_ngay = int.parse(end_dd) - int.parse(start_dd);
    var leng_thang = int.parse(end_mm) - int.parse(start_mm);
    var leng_nam = int.parse(end_yy) - int.parse(start_yy);
    if ((leng_nam < 0) ||
        (leng_nam == 0 && leng_thang < 0) ||
        (leng_thang == 0 && leng_ngay < 0)) {
      //thang sau < thang trc
      EasyLoading.showToast("Khoảng thời gian không hợp lệ");
      return;
    } else {
      var stringStart = start_year + "-" + start_mm + "-" + start_dd;
      var stringEnd = end_year + "-" + end_mm + "-" + end_dd;

      final start123 = DateTime.tryParse(stringStart);
      final end123 = DateTime.tryParse(stringEnd);
      //final difference = end123.difference(start123).inDays;
      //var leng_view = difference+1;
      var arrDays = getDaysInBeteween(start123, end123);
      //print('leng_view :${arrDays.length}');
      //print('leng_view :${arrDays}');

      var arr_imei = [];
      // double sum = 0;
      for (int i = 0; i < arrDays.length; i++) {
        var day1 = arrDays[i].toString().substring(8, 10);
        var month1 = arrDays[i].toString().substring(5, 7);
        var year1 = arrDays[i].toString().substring(2, 4);
        var elementId =
            imei + "_" + day1.padLeft(2, '0') + month1.padLeft(2, '0') + year1;
        arr_imei.add(elementId);
      }
      //print('arr :${arr_imei}');

      var cnt_null = 0;
      value_tongsanluong = 0;
      setState(() {
        viewNgayClicked = false;
        viewThangClicked = false;
        viewNamClicked = false;
        viewTuychonClicked = true;
        desktopSalesData.clear();
        seriesList = _createRadomData();
      });
      for (int i = 0; i < arr_imei.length; i++) {
        show_btn = false;

        //print('data of imei i: ${arr_imei[i]}');
        await bloc.viewChart(arr_imei[i]).then((error) {
          setState(() {
            if (error != null) {
              cnt_null++;
              if (cnt_null == arr_imei.length) {
                desktopSalesData.clear();
                EasyLoading.showToast("Không có dữ liệu");
              }
              //EasyLoading.showToast(error);
              return;
            } else {
              value = bloc.dataChart;

              //for (int i = 0; i < value[0].valueMesument.length; i++) {
              // chartData.add(new ChartData(double.parse(i.toString()), double.parse(value[0].valueMesument[i].pOut)));
              var leng_dataInDay = value[0].valueMesument.length;
              var x = value[0]
                  .valueMesument[leng_dataInDay - 1]
                  .time
                  .substring(0, 5);
              var y = double.parse(
                  value[0].valueMesument[leng_dataInDay - 1].eToday);
              //print('data x: ${x.toString()}');
              //print('data y: ${y.toString()}');
              value_tongsanluong = value_tongsanluong + y;
              desktopSalesData.add(DataShow(x, y));
              //}
            }
            seriesList = _createRadomData();
          }); //
        });
      }
      // print('data sum= : ${value_tongsanluong}');
      show_btn = true;
    }
  }
  void getInfo(String imei){
    if (imei != null) {
      blocInfo.showInfo(imei).then((error) {
        if (error != null) {
          EasyLoading.showToast(error);
        } else {
          infos = blocInfo.viewInfo;
        }
      });
    };
  }
  void getValue(String username) async {
    if(await isInternet() == false) return;
    getInfo(imei);
     setState(() {
       value_capnhat = null;
       errors = [];
       errors2 = [];
    if (username != null) {
      bloc1.viewDevice(username).then((error) {
        if (error != null) {
          EasyLoading.showToast(error);
        } else {
          value1 = bloc1.userViewDevice;
          for(int i=0;i<value1.lists.length;i++){
            //print('i: ${value1.lists[i].imei}');
            //print('j: ${imei}');
            if(value1.lists[i].imei.indexOf(imei) >= 0){
              if(mounted){
                setState(() {
                  value_capnhat = value1.lists[i];

                  errors = value1.listerrs;
                  for (int i = 0; i < errors.length; i++) {
                    Listerr ele;
                    if (errors[i].imei.indexOf(imei) >= 0 && errors[i].imei.length == 19) {

                      var day = errors[i].time.substring(0,2);

                      var month = errors[i].time.substring(3,5);
                      var year = "20"+errors[i].time.substring(6,8);
                      var hh = errors[i].time.substring(9,11);
                      var mm = errors[i].time.substring(12,14);
                      var ss = errors[i].time.substring(15,17);
                      var formatted = '$year-$month-$day $hh:$mm:$ss';

                      var time2 = DateTime.parse(formatted);
                      // arr.add(DateTime.parse(formatted));
                      errors[i].time2 = time2.toString();
                      errors2.add(errors[i]);
                    }
                  }

                  //sap xep list err
                  errors2.sort((a, b) {
                    var adate = a.time2; //before -> var adate = a.expiry;
                    var bdate = b.time2; //var bdate = b.expiry;
                    return -adate.compareTo(bdate);
                  });

                });
              }

              //print('OK: ${value1.lists[i].imei}');
            }
          }
        }
      });
    }

     });
  }


}

class ChartData {
  ChartData(this.x, this.y);

  final String x;
  final double y;
}

List<DateTime> getDaysInBeteween(DateTime startDate, DateTime endDate) {
  List<DateTime> days = [];
  for (int i = 0; i <= endDate.difference(startDate).inDays; i++) {
    days.add(startDate.add(Duration(days: i)));
  }
  return days;
}

class DataShow {
  final String time;
  final double db;

  DataShow(this.time, this.db);
}

int daysInMonth(DateTime date) {
  var firstDayThisMonth = new DateTime(date.year, date.month, date.day);
  var firstDayNextMonth = new DateTime(firstDayThisMonth.year,
      firstDayThisMonth.month + 1, firstDayThisMonth.day);
  return firstDayNextMonth.difference(firstDayThisMonth).inDays;
}

class AppStaticVar {
  static String valueDisplay = "";
}
