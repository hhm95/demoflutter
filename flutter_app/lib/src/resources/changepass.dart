// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_app/src/blocs/changepass_bloc.dart';
import 'package:flutter_app/src/blocs/login_bloc.dart';
import 'package:flutter_app/src/blocs/newdevice_bloc.dart';
import 'package:flutter_app/src/others/EasyJsonParse/Users.dart';
import 'package:flutter_app/src/resources/home_page.dart';
import 'package:flutter_app/src/resources/view_device.dart';
import 'dart:developer';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'login_page.dart';

// ignore: must_be_immutable
class ChangePass extends StatefulWidget {

  String username;
  ChangePass({this.username});

  @override
  _ChangePassState createState() => _ChangePassState(username);
}

class _ChangePassState extends State<ChangePass> {

  String username;
  ChangePassBloc bloc = new ChangePassBloc();
  TextEditingController _oldPassController = new TextEditingController();
  TextEditingController _newPassController = new TextEditingController();
  TextEditingController _newPass2Controller = new TextEditingController();
  _ChangePassState(this.username);

  @override

  void initState() {
    // TODO: implement initState
    //goi api lay ve doi tuong device => ve do thi
    // EasyLoading.showToast(username);

    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement buil;
    return Scaffold(
      appBar: AppBar(
        title: Text("Đổi mật khẩu"),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          width: MediaQuery
              .of(context)
              .size
              .width,
          height: MediaQuery
              .of(context)
              .size
              .height,
          padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
          //constraints: BoxConstraints.expand(),
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 60,),
              Center(
                child: Container(
                    width: 70,
                    height: 70,
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(0xffd8d8d8),
                    ),
                    child: FlutterLogo()),
              ),
              SizedBox(height: 30,),
              Center(child: Text(
                  "Đổi mật khẩu",
                  style: TextStyle(fontSize: 30))),

              SizedBox(height: 20,),
              Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                  child: StreamBuilder(
                      stream: bloc.oldPassStream,
                      builder: (context, AsyncSnapshot<String>snapshot) {
                        String error;
                        if (snapshot.hasData) {
                          error = snapshot.data;
                        }
                        return TextField(
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          controller: _oldPassController,
                          decoration: InputDecoration(
                              labelText: "Mật khẩu cũ",
                              errorText: error,
                              labelStyle:
                              TextStyle(
                                  color: Color(0xff888888), fontSize: 15)),
                        );
                      })),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                child: StreamBuilder(
                  stream: bloc.newPassStream,
                  builder: (context, AsyncSnapshot<String>snapshot) {
                    String error;
                    if (snapshot.hasData) {
                      error = snapshot.data;
                    }
                    return TextField(
                      style: TextStyle(fontSize: 18, color: Colors.black),
                      controller: _newPassController,
                      decoration: InputDecoration(
                          labelText: "Mật khẩu mới",
                          errorText: error,
                          labelStyle:
                          TextStyle(
                              color: Color(0xff888888), fontSize: 15)),
                    );
                  },
                ),
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                  child: StreamBuilder(
                      stream: bloc.newPass2Stream,
                      builder: (context, AsyncSnapshot<String>snapshot) {
                        String error;
                        if (snapshot.hasData) {
                          error = snapshot.data;
                        }
                        return TextField(
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          controller: _newPass2Controller,
                          decoration: InputDecoration(
                              labelText: "Nhập lại",
                              errorText: error,
                              labelStyle:
                              TextStyle(
                                  color: Color(0xff888888), fontSize: 15)),
                        );
                      })),

              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: SizedBox(
                  width: double.infinity,
                  height: 56,
                  child: RaisedButton(
                    color: Colors.blue,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    onPressed: onchangePassClicked,
                    child: Text("Thay đổi", style: TextStyle(color: Colors
                        .white, fontSize: 16),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onchangePassClicked() async {
    final String oldPass = _oldPassController.text;
    final String newPass = _newPassController.text;
    final String newPass2 = _newPass2Controller.text;

    bloc.changePassword(username, oldPass, newPass, newPass2).then((error){
      if(error != null){
        EasyLoading.showToast(error);
      }else{
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ViewDevice(username: username),
            ));
      }
    });
  }

}


