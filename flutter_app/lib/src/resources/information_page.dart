// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_app/src/blocs/editinfodevice_bloc.dart';
import 'package:flutter_app/src/blocs/login_bloc.dart';
import 'package:flutter_app/src/blocs/newdevice_bloc.dart';
import 'package:flutter_app/src/others/EasyJsonParse/Users.dart';
import 'package:flutter_app/src/others/EasyJsonParse/ViewInfo.dart';
import 'package:flutter_app/src/resources/home_page.dart';
import 'package:flutter_app/src/resources/view_chart.dart';
import 'package:flutter_app/src/resources/view_device.dart';
import 'dart:developer';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'login_page.dart';

// ignore: must_be_immutable
class InfoPage extends StatefulWidget {
  List<ViewInfo> infos;
  String imei;
  String username;

  InfoPage({this.infos, this.imei, this.username});

  @override
  _InfoPageState createState() => _InfoPageState(infos, imei, username);
}

class _InfoPageState extends State<InfoPage> {
  List<ViewInfo> infos;
  String imei;
  String username;
  DateTime _dateFrom;
  DateTime _dateTo;

  bool _isShowFilter = false;
  bool show_btn = true;
  bool checkedValue = true;

  _InfoPageState(this.infos, this.imei, this.username);

  @override
  void initState() {
    // TODO: implement initState
    //goi api lay ve doi tuong device => ve do thi
    //EasyLoading.showToast(infos[0].vOutMax);

    if (username != null) {
      EasyLoading.showToast("Name null");
    }
    EasyLoading.showToast(username);
    // if (imei != null) {
    //   EasyLoading.showToast("Name null");
    // }
    // EasyLoading.showToast(imei);
    // if (infos != null) {
    //   EasyLoading.showToast("infos null");
    // }
    // EasyLoading.showToast(infos[0].imei);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement buil;
    return Scaffold(
      appBar: AppBar(
        title: Text("Thông tin thiết bị"),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            Center(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: new Text(imei,
                    style: TextStyle(color: Colors.blue, fontSize: 20)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
              child: Container(
                child: Row(
                  children: <Widget>[
                    Flexible(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 50),
                        child: Center(child: Image.asset('assets/set.png')),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                        child: Container(
                          child: Column(
                            children: [
                              Container(
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      child: new Text(
                                        "Tên:",
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 18),
                                      ),
                                    ),
                                    Container(
                                      child: new Text(
                                        "Hướng góc:",
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 18),
                                      ),
                                    ),
                                    Container(
                                      child: new Text(
                                        "Số lượng Pin:",
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 18),
                                      ),
                                    ),
                                    Container(
                                      child: new Text(
                                        "Vout:",
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 18),
                                      ),
                                    ),
                                    Container(
                                      child: new Text(
                                        "Vout_max:",
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 18),
                                      ),
                                    ),
                                    Container(
                                      child: new Text(
                                        "Hiệu suất:",
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 18),
                                      ),
                                    ),
                                    Container(
                                      child: new Text(
                                        "Version:",
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 18),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      child: (username == "admin")
                                          ? FlatButton()
                                          : Container(
                                              height: 40,
                                            ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: Container(
                              child: Column(
                                children: [
                                  Container(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                          child: new Text(
                                            infos[0].name,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18),
                                          ),
                                        ),
                                        Container(
                                          child: new Text(
                                            infos[0].direction,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18),
                                          ),
                                        ),
                                        Container(
                                          child: new Text(
                                            infos[0].valuePin,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18),
                                          ),
                                        ),
                                        Container(
                                          child: new Text(
                                            infos[0].vOut,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18),
                                          ),
                                        ),
                                        Container(
                                          child: new Text(
                                            infos[0].vOutMax,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18),
                                          ),
                                        ),
                                        Container(
                                          child: new Text(
                                            infos[0].performance,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18),
                                          ),
                                        ),
                                        Container(
                                          child: new Text(
                                            infos[0].sfVsDevice,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 0, 0, 0),
                                          child: (username == "admin")
                                              ? RaisedButton(
                                                  color: Colors.blue,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  10))),
                                                  onPressed: () {
                                                    onEditClick(infos);
                                                  },
                                                  child: Text(
                                                    "Sửa",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 16),
                                                  ),
                                                )
                                              : Container(
                                                  height: 40,
                                                ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: new Text("Bảng sản lượng",
                    style: TextStyle(color: Colors.blue, fontSize: 20)),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void onEditClick(List<ViewInfo> infos) {
    //EasyLoading.showToast(imei);
    Navigator.push(
        context,
        MaterialPageRoute(
        ));
  }
}
