// @dart=2.9
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/src/blocs/get_location.dart';
import 'dart:math' as math;
import 'package:flutter_app/src/blocs/viewdevice_bloc.dart';
import 'package:flutter_app/src/others/EasyJsonParse/GetLocation.dart';
import 'package:flutter_app/src/others/EasyJsonParse/JsonParseDemo.dart';
import 'package:flutter_app/src/others/EasyJsonParse/UserViewDevice.dart';
import 'package:flutter_app/src/others/EasyJsonParse/Users.dart';
import 'package:flutter_app/src/resources/add_device.dart';
import 'package:flutter_app/src/resources/changepass.dart';
import 'package:flutter_app/src/resources/login_page.dart';
import 'package:flutter_app/src/resources/view_chart.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_gsmcelllocation/flutter_gsmcelllocation.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';

class ViewDevice extends StatefulWidget {
  String username;
  UserViewDevice value;
  Device device;

  ViewDevice({this.username});

  @override
  _ViewDeviceState createState() => _ViewDeviceState(username);
}

class _ViewDeviceState extends State<ViewDevice> {
  String username;
  UserViewDevice value;
  GetLocation infoLocation;
  ViewDeviceBloc bloc = new ViewDeviceBloc();
  GetLocationBloc blocLocation = new GetLocationBloc();
  Device device;
  String timeString;
  Timer timer;
  Position _currentPosition;
  String _currentAddress;
  dynamic  _platformVersion;
  bool get1 = true;
  _ViewDeviceState(this.username);

  @override
  void initState() {
    // TODO: implement initState
    //goij api lay ve doi tuong device => ve do thi
    // EasyLoading.showToast(username);

    getValueUser(username);
    initPlatformState();
    EasyLoading.showToast(_platformVersion);

    // var telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
    // var cellLocation = (GsmCellLocation)telephonyManager.getCellLocation();
    // var cid = cellLocation.getCid();
    // var lac = cellLocation.getLac();

    timeString = _formatDateTime(DateTime.now());
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    bloc.dispose();
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Tổng quan"),
        ),
        body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  StreamBuilder<UserViewDevice>(
                      stream: bloc.streamDevices,
                      builder:
                          (context, AsyncSnapshot<UserViewDevice> snapshot) {
                        if (!snapshot.hasData) {
                          return Container(
                            child: Center(
                              child: Text("Đang tải dữ liệu ..."),
                            ),
                          );
                        }
                        return screenViewDevice(snapshot.data);
                      }),
                  // Container(
                  //   height: 4,
                  //   color: Colors.black,
                  // ),
                ],
              ),
            )));
  }

  Widget screenViewDevice(UserViewDevice value) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 10, 20, 40),
      // height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  FlatButton(
                    onPressed: () => changePassword(username),
                    child: Text(
                      "Đổi mật khẩu",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        decoration: TextDecoration.underline,
                        decorationStyle: TextDecorationStyle.double,
                      ),
                    ),
                    // color: Colors.blue[600],
                  ),
                  FlatButton(
                    // shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                    onPressed: logoutClicked,
                    child: Text(
                      "Đăng xuất",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        decoration: TextDecoration.underline,
                        decorationStyle: TextDecorationStyle.double,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            color: const Color(0xff),
            child: Text(
              timeString,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 24, color: Colors.black),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            color: const Color(0xff),
            child: Text(
              "Thông tin hoạt động",
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 24, color: Colors.black),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 15, 0, 2),
            color: Colors.blue[500],
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                // Expanded(
                //     flex: 4,
                //     child: Text(
                //       'S/N',
                //       textAlign: TextAlign.center,
                //       style: TextStyle(
                //         fontSize: 16,
                //         fontWeight: FontWeight.bold,
                //         color: Colors.white,
                //       ),
                //     )),
                // Container(
                //   height: 50,
                //   width: 2.0,
                //   color: Colors.white,
                // ),
                Expanded(
                    flex: 4,
                    child: Text(
                      'Tên thiết bị',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    )),
                Container(
                  height: 50,
                  width: 2.0,
                  color: Colors.white,
                ),
                Expanded(
                    flex: 3,
                    child: Text(
                      'TTGHĐ (giờ)',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    )),
                Container(
                  height: 50,
                  width: 2.0,
                  color: Colors.white,
                ),
                Expanded(
                    flex: 3,
                    child: Text(
                      'SLLK (kWh)',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    )),
                Container(
                  height: 50,
                  width: 2.0,
                  color: Colors.white,
                ),
                Expanded(
                    flex: 3,
                    child: Text(
                      'SL Ngày (kWh)',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    )),
              ],
            ),
          ),
          Container(
            height: value.lists.length < 1
                ? 50.0
                : ((50 + 51 * value.lists.length) >
                        MediaQuery.of(context).size.height / 4)
                    ? MediaQuery.of(context).size.height / 4
                    : (50 + 51 * value.lists.length).toDouble(),
            constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height / 3),
            child: CupertinoScrollbar(
              child: ListView.builder(
                  itemCount: value.lists.length,
                  itemBuilder: (context, index) {
                    return Container(
                      height: 50,
                      margin: const EdgeInsets.fromLTRB(0, 1, 0, 1),
                      color: Colors.blue[200],
                      //Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(0.6),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          // Expanded(
                          //     flex: 4,
                          //     child: Container(
                          //         margin:
                          //             const EdgeInsets.fromLTRB(10, 0, 5, 0),
                          //         child: RaisedButton(
                          //           onPressed: () {
                          //             viewDevice(
                          //                 value.lists[index].imei
                          //                     .substring(0, 12),
                          //                 username,
                          //                 value.lists[index],
                          //                 value.listerrs,
                          //                 password);
                          //           },
                          //           child: Text(
                          //               //value.lists[index].imei.substring(0, 12),
                          //               value.lists[index].imei
                          //                   .substring(0, 12),
                          //               style: TextStyle(
                          //                   fontSize: 12,
                          //                   color: Colors.black,
                          //                   fontWeight: FontWeight.bold)),
                          //         ))),
                          // Container(
                          //   height: 50,
                          //   width: 2.0,
                          //   color: Colors.white,
                          // ),
                          Expanded(
                              flex: 4,
                              child: Container(
                                  margin:
                                      const EdgeInsets.fromLTRB(10, 0, 5, 0),
                                  child: RaisedButton(
                                    onPressed: () {
                                      viewDevice(
                                          value.lists[index].imei
                                              .substring(0, 12),
                                          username,
                                          value.lists[index],
                                          value.listerrs,
                                          );
                                    },
                                    child: Text(value.lists[index].name,
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold)),
                                  ))),
                          Container(
                            height: 50,
                            width: 2.0,
                            color: Colors.white,
                          ),
                          Expanded(
                              flex: 3,
                              child: Text(
                                value.lists[index].ttg,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              )),
                          Container(
                            height: 50,
                            width: 2.0,
                            color: Colors.white,
                          ),
                          Expanded(
                              flex: 3,
                              child: Text(
                                value.lists[index].sllk,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              )),
                          Container(
                            height: 50,
                            width: 2.0,
                            color: Colors.white,
                          ),
                          Expanded(
                              flex: 3,
                              child: Text(
                                value.lists[index].etoday == null
                                    ? null
                                    : value.lists[index].etoday,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              )),
                        ],
                      ),
                    );
                  }),
            ),
          ),
          // Container(
          //   alignment: Alignment.centerLeft,
          //   margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          //   color: const Color(0xff),
          //   child: Text(
          //     "Thông tin chi tiết",
          //     textAlign: TextAlign.left,
          //     style: TextStyle(fontSize: 24, color: Colors.black),
          //   ),
          // ),
          // Container(
          //   margin: const EdgeInsets.fromLTRB(0, 15, 0, 2),
          //   color: Colors.blue[500],
          //   child: Row(
          //     mainAxisSize: MainAxisSize.max,
          //     children: [
          //       Expanded(
          //           flex: 4,
          //           child: Text(
          //             'S/N',
          //             textAlign: TextAlign.center,
          //             style: TextStyle(
          //               fontSize: 16,
          //               fontWeight: FontWeight.bold,
          //               color: Colors.white,
          //             ),
          //           )),
          //       Container(
          //         height: 50,
          //         width: 2.0,
          //         color: Colors.white,
          //       ),
          //       Expanded(
          //           flex: 3,
          //           child: Text(
          //             'Tên thiết bị',
          //             textAlign: TextAlign.center,
          //             style: TextStyle(
          //               fontSize: 16,
          //               fontWeight: FontWeight.bold,
          //               color: Colors.white,
          //             ),
          //           )),
          //       Container(
          //         height: 50,
          //         width: 2.0,
          //         color: Colors.white,
          //       ),
          //       Expanded(
          //           flex: 3,
          //           child: Text(
          //             'Tùy chọn',
          //             textAlign: TextAlign.center,
          //             style: TextStyle(
          //               fontSize: 16,
          //               fontWeight: FontWeight.bold,
          //               color: Colors.white,
          //             ),
          //           )),
          //     ],
          //   ),
          // ),
          // Container(
          //     height: value.devices.length < 1
          //         ? 50.0
          //         : ((50 + 51 * value.devices.length) >
          //                 MediaQuery.of(context).size.height / 4)
          //             ? MediaQuery.of(context).size.height / 4
          //             : (50 + 51 * value.devices.length).toDouble(),
          //     child: Scrollbar(
          //       child: ListView.builder(
          //           itemCount: value.devices.length,
          //           itemBuilder: (context, index) {
          //             return Container(
          //               color: Colors.blue[200],
          //               margin: const EdgeInsets.fromLTRB(0, 1, 0, 1),
          //               height: 50,
          //               child: Row(
          //                 mainAxisSize: MainAxisSize.max,
          //                 children: [
          //                   Expanded(
          //                       flex: 4,
          //                       child: Container(
          //                         margin: const EdgeInsets.fromLTRB(2, 0, 2, 0),
          //                         child: Text(value.devices[index].imei,
          //                             textAlign: TextAlign.center,
          //                             style: TextStyle(
          //                               fontSize: 12,
          //                               color: Colors.black,
          //                               fontWeight: FontWeight.bold,
          //                             )),
          //                       )),
          //                   Container(
          //                     height: 50,
          //                     width: 2.0,
          //                     color: Colors.white,
          //                   ),
          //                   Expanded(
          //                       flex: 3,
          //                       child: Text(
          //                         value.devices[index].deviceName,
          //                         textAlign: TextAlign.center,
          //                       )),
          //                   Container(
          //                     height: 50,
          //                     width: 2.0,
          //                     color: Colors.white,
          //                   ),
          //                   Expanded(
          //                     flex: 3,
          //                     child: Container(
          //                       margin:
          //                           const EdgeInsets.fromLTRB(10, 10, 10, 10),
          //                       child: RaisedButton(
          //                         onPressed: () {
          //                           deleteDevice(value.username,
          //                               value.devices[index].imei);
          //                         },
          //                         child: Text("Delete"),
          //                       ),
          //                     ),
          //                   ),
          //                 ],
          //               ),
          //             );
          //           }),
          //     )),
          //
          // Container(
          //   alignment: Alignment.centerLeft,
          //   margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          //   color: const Color(0xff),
          //   child: Text(
          //     "Lịch sử lỗi",
          //     textAlign: TextAlign.left,
          //     style: TextStyle(fontSize: 24, color: Colors.black),
          //   ),
          // ),
          // Container(
          //   margin: const EdgeInsets.fromLTRB(0, 15, 0, 2),
          //   color: Colors.blue[500],
          //   child: Row(
          //     mainAxisSize: MainAxisSize.max,
          //     children: [
          //       Expanded(
          //           flex: 4,
          //           child: Text(
          //             'S/N',
          //             textAlign: TextAlign.center,
          //             style: TextStyle(
          //               fontSize: 16,
          //               fontWeight: FontWeight.bold,
          //               color: Colors.white,
          //             ),
          //           )),
          //       Container(
          //         height: 50,
          //         width: 2.0,
          //         color: Colors.white,
          //       ),
          //       Expanded(
          //           flex: 3,
          //           child: Text(
          //             'Lỗi',
          //             textAlign: TextAlign.center,
          //             style: TextStyle(
          //               fontSize: 16,
          //               fontWeight: FontWeight.bold,
          //               color: Colors.white,
          //             ),
          //           )),
          //     ],
          //   ),
          // ),
          // Container(
          //     height: value.listerrs.length < 1
          //         ? 50.0
          //         : ((50 + 51 * value.listerrs.length) >
          //                 MediaQuery.of(context).size.height / 4)
          //             ? MediaQuery.of(context).size.height / 4
          //             : (50 + 51 * value.listerrs.length).toDouble(),
          //     child: Scrollbar(
          //       child: ListView.builder(
          //           itemCount: value.listerrs.length,
          //           itemBuilder: (context, index) {
          //             return Container(
          //               color: Colors.blue[200],
          //               margin: const EdgeInsets.fromLTRB(0, 1, 0, 1),
          //               //Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(0.6),
          //               height: 50,
          //               child: Row(
          //                 mainAxisSize: MainAxisSize.max,
          //                 children: [
          //                   Expanded(
          //                       flex: 4,
          //                       child: Container(
          //                         margin:
          //                             const EdgeInsets.fromLTRB(10, 0, 2, 0),
          //                         child: Text(value.listerrs[index].imei,
          //                             style: TextStyle(
          //                                 fontSize: 12,
          //                                 color: Colors.black,
          //                                 fontWeight: FontWeight.bold)),
          //                       )),
          //                   Container(
          //                     height: 50,
          //                     width: 2.0,
          //                     color: Colors.white,
          //                   ),
          //                   Expanded(
          //                       flex: 3,
          //                       child: Text(
          //                         value.listerrs[index].errcode,
          //                         textAlign: TextAlign.center,
          //                       )),
          //                 ],
          //               ),
          //             );
          //           }),
          //     )),
          Container(
            height: 10,
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            color: const Color(0xff),
            child: Text(
              "Trạng thái",
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 24, color: Colors.black),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 15, 0, 2),
            color: Colors.blue[500],
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                    height: 40,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                      child: Center(
                        child: Text(
                          'Thông tin',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )),
              ],
            ),
          ),
          Container(
              height: value.historys.length < 1
                  ? 50.0
                  : ((50 + 51 * value.historys.length) >
                          MediaQuery.of(context).size.height / 4)
                      ? MediaQuery.of(context).size.height / 4
                      : (50 + 51 * value.historys.length).toDouble(),
              child: Scrollbar(
                child: ListView.builder(
                    itemCount: value.historys.length,
                    itemBuilder: (context, index) {
                      return Container(
                        color: Colors.blue[200],
                        margin: const EdgeInsets.fromLTRB(0, 1, 0, 1),
                        //Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(0.6),
                        height: 50,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Expanded(
                                flex: 4,
                                child: Container(
                                  margin:
                                      const EdgeInsets.fromLTRB(10, 0, 2, 0),
                                  child: Text(value.historys[index],
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold)),
                                )),
                          ],
                        ),
                      );
                    }),
              )),

          // Center(
          //   child: Padding(
          //     padding: const EdgeInsets.all(8.0),
          //     child: Container(
          //       alignment: Alignment.centerRight,
          //       child: IconButton(
          //           icon: Icon(Icons.add_circle,color:Colors.blue,size: 50,),
          //           onPressed:addNewDeviceClicked,
          //       ),
          //       // child: RaisedButton.icon(
          //       //   onPressed: addNewDeviceClicked,icon:Icon(Icons.add_circle,color:Colors.blue,),label: Text(""),
          //       //   // color: Colors.blue[600],
          //       // ),
          //     ),
          //   ),
          // ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  alignment: Alignment.centerRight,
                  child: IconButton(
                    icon: Icon(
                      Icons.refresh,
                      color: Colors.blue,
                      size: 50,
                    ),
                    onPressed: () {
                      getValueUser(username);
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  alignment: Alignment.centerRight,
                  child: IconButton(
                    icon: Icon(
                      Icons.add_circle,
                      color: Colors.blue,
                      size: 50,
                    ),
                    onPressed: addNewDeviceClicked,
                  ),
                ),
              ),

            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              alignment: Alignment.centerRight,
              child: IconButton(
                icon: Icon(
                  Icons.location_on,
                  color: Colors.blue,
                  size: 50,
                ),
                onPressed: getLocation,
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Text(
                _currentAddress == null? '...' : _currentAddress, textAlign: TextAlign.center,
              ),
            ),
          ),
          get1?
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: SelectableText(
                _currentPosition == null? '...' : _currentPosition.latitude.toString() +','+ _currentPosition.longitude.toString()
              ),
            ),
          ):
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: SelectableText(
                  infoLocation == null? '...' : infoLocation.lat.toString() +','+ infoLocation.lon.toString()
              ),
            ),
          )
        ],
      ),
    );
  }

  void addNewDeviceClicked() {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              AddDevice(username: value.username),
        ));
  }

  void getLocation()async{
    _currentPosition = null;
    _currentAddress = 'waiting';
    if (await Geolocator.isLocationServiceEnabled()) {
      setState(() {
        get1 = true;
      });
      EasyLoading.showToast('GPS on');
      await _getCurrentLocation();
      if (_currentPosition != null) {
        print('LAT: ${_currentPosition.latitude}, LNG: ${_currentPosition
            .longitude}');
        print('address: $_currentAddress');
        return;
      }
    }else if(await isInternet()){
      setState(() {
         get1 = false;
      });
        EasyLoading.showToast('GPS off + INTERNET on');
        whenGPSoff();
        return;
      }else{
        EasyLoading.showToast('GPS + INTERNET off');
        return;
      }
    }
    // if(await isInternet()){
    //   EasyLoading.showToast('INTERNET');
    //   print('has internet');
    //   _getCurrentLocation();
    //   if(_currentPosition != null){
    //     print('LAT: ${_currentPosition.latitude}, LNG: ${_currentPosition.longitude}');
    //     print('address: $_currentAddress');
    //
    //   }
    // }else{
    //   EasyLoading.showToast('NO INTERNET');
    //   print('no internet');
    //   _getCurrentLocation();
    //   if(_currentPosition != null){
    //     print('LAT: ${_currentPosition.latitude}, LNG: ${_currentPosition.longitude}');
    //     print('address: $_currentAddress');
    //
    //   }
    // }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.deniedForever) {
        // Permissions are denied forever, handle appropriately.
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }

      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error(
            'Location permissions are denied');
      }
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  // _getCurrentLocation() {
  //   Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best, forceAndroidLocationManager: true)
  //       .then((Position position) {
  //     setState(() {
  //       _currentPosition = position;
  //       _getAddressFromLatLng();
  //     });
  //   }).catchError((e) {
  //     print(e);
  //   });
  // }

  _getCurrentLocation(){
    _determinePosition().then((value){
      setState(() {
        _currentPosition = value;
        print('get current location ok');
        _getAddressFromLatLng(_currentPosition.latitude, _currentPosition.longitude);
      });
    });

  }

  _getAddressFromLatLng(double lat, double lon) async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(lat,lon
          // _currentPosition.latitude,
          // _currentPosition.longitude
      );

      Placemark place = placemarks[0];

      setState(() {
        _currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  void whenGPSoff() async {
    String mcc = "452";
    String mnc = "4";
    String lac = "41054";
    String cid = "15603";
    blocLocation.getLocbyM(mcc,mnc,lac,cid).then((error) {
      if (error != null) {
        EasyLoading.showToast(error);
      } else {
        setState(() {
          print('has data');
          // EasyLoading.showToast('clicked has data');
          infoLocation = blocLocation.viewInfo;
          //print data
          print(infoLocation.lat.toString());
          print(infoLocation.lon.toString());
          _currentAddress = infoLocation.address;
        });
      }
    });
  }

  Future<void> initPlatformState() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      print("START");
      _platformVersion = await FlutterGsmcelllocation.getGsmCell;
      print('plat '+_platformVersion.toString());
    } on PlatformException {
      _platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = _platformVersion;
    });
  }

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    setState(() {
      timeString = formattedDateTime;
    });
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat('dd/MM/yyyy hh:mm:ss').format(dateTime);
  }

  void deleteDevice(String username, String imei) {
    //post
    //EasyLoading.showToast(username + imei);

    bloc.delete(username, imei).then((error) {
      if (error != null) {
        EasyLoading.showToast(error);
      } else {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ViewDevice(username: bloc.user_delete),
            ));
      }
    });
  }

  void getValueUser(String username) async{
    if(await isInternet() == false) return;
    if (username != null) {
      bloc.viewDevice(username).then((error) {
        if (error != null) {
          EasyLoading.showToast(error);
        } else {
          setState(() {
            // EasyLoading.showToast('clicked has data');
            value = bloc.userViewDevice;
          });
        }
      });
    }
  }

  void viewDevice(String imei, String username, ListElement value, List<Listerr> errors) async {
    List<Listerr> err = [];
    List<Listerr> err2 =[];

    //check internet
    if(await isInternet() == false) return;

    for (int i = 0; i < errors.length; i++) {
      Listerr ele;
      if (errors[i].imei.indexOf(imei) >= 0 && errors[i].imei.length == 19) {

        var day = errors[i].time.substring(0,2);

        var month = errors[i].time.substring(3,5);
        var year = "20"+errors[i].time.substring(6,8);
        var hh = errors[i].time.substring(9,11);
        var mm = errors[i].time.substring(12,14);
        var ss = errors[i].time.substring(15,17);
        var formatted = '$year-$month-$day $hh:$mm:$ss';

        var time2 = DateTime.parse(formatted);
        // arr.add(DateTime.parse(formatted));
        errors[i].time2 = time2.toString();
        err2.add(errors[i]);
      }
    }

    //sap xep list err
    err2.sort((a, b) {
      var adate = a.time2; //before -> var adate = a.expiry;
      var bdate = b.time2; //var bdate = b.expiry;
      return -adate.compareTo(bdate);
    });

    var result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ViewChart(
              imei: imei,
              username: username,
              datas: value),
        ));
    if (result != null) {
      getValueUser(username);
    }
  }

  void logoutClicked() {
    // Navigator.push(
    //     context,
    //     MaterialPageRoute(
    //       builder: (context) => LoginPage(),
    //     ));
    //Navigator.pop(context,true);
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
  }

  void changePassword(String username) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ChangePass(username: username),
        ));
  }
}

isInternet() {
}
