// @dart=2.9
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_app/src/blocs/login_bloc.dart';
import 'package:flutter_app/src/blocs/signup_bloc.dart';
import 'package:flutter_app/src/others/EasyJsonParse/Users.dart';
import 'package:flutter_app/src/resources/home_page.dart';
import 'package:flutter_app/src/resources/login_page.dart';
import 'package:flutter_app/src/resources/view_device.dart';

import 'dart:developer';

import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../../environment.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  SignUpBloc bloc = new SignUpBloc();
  bool _showPass = false;
  bool _showLienKet = false;
  TextEditingController _userController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _otpController = new TextEditingController();
  TextEditingController _linkController = new TextEditingController(text: 'http://117.1.17.14:7500/');
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement buil;
    return Scaffold(
      appBar: AppBar(
        title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children:[
              Text("Luna JR"+" Profile"),
              IconButton(
                icon: Icon(
                  Icons.more_vert,
                  color: Colors.white,
                  size: 35,
                ),
                onPressed: () {

                },
              ),
            ]),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          width: MediaQuery.of(context).size.width,
          //height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
          //constraints: BoxConstraints.expand(),
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: Container(
                  height: 120,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children:[
                              Expanded(
                                child: InkWell(
                                  onTap: () {},
                                  child: Container(
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20.0),
                                      child: Image.asset('assets/images/pic31.png'),
                                    ),),
                                ),
                              ),
                              Text("Luna JR",style: TextStyle(fontSize: 18,color: Colors.black,fontWeight: FontWeight.bold),),
                              Text("Digital Artist",style: TextStyle(fontSize: 18,color: Colors.grey,)),
                            ]),
                      ),
                      Expanded(
                        flex: 3,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children:[
                              Padding(
                                padding: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                child: FlatButton(
                                  onPressed: null,
                                  child: Row(
                                    children:[
                                      IconButton(
                                        icon: Icon(
                                          Icons.check_box_rounded,
                                          color: Colors.blue,
                                          size: 25,
                                        ),
                                      ),
                                      Text('   Following', style: TextStyle(
                                          color: Colors.blue
                                      )
                                      ),],
                                  ),
                                  textColor: Color.alphaBlend(Colors.red, Colors.black),
                                  shape: RoundedRectangleBorder(side: BorderSide(
                                      color: Colors.blue,
                                      width: 1,
                                      style: BorderStyle.solid
                                  ), borderRadius: BorderRadius.circular(10)),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                child: FlatButton(
                                  onPressed: null,
                                  child: Row(
                                    children:[
                                      IconButton(
                                        icon: Icon(
                                          Icons.email_outlined,
                                          color: Colors.blue,
                                          size: 25,
                                        ),
                                      ),
                                      Text('   Message', style: TextStyle(
                                          color: Colors.blue
                                      )
                                      ),],
                                  ),
                                  textColor: Color.alphaBlend(Colors.red, Colors.black),
                                  shape: RoundedRectangleBorder(side: BorderSide(
                                      color: Colors.blue,
                                      width: 1,
                                      style: BorderStyle.solid
                                  ), borderRadius: BorderRadius.circular(10)),
                                ),
                              )

                            ]
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                          children:[
                            Text("123",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                            Text("captures"),
                          ]),
                    ),
                    Expanded(
                      child: Column(
                          children:[
                            Text("456",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                            Text("followers"),
                          ]),
                    ),
                    Expanded(
                      child: Column(
                          children:[
                            Text("789",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                            Text("following"),
                          ]),
                    ),
                  ],),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 0, 20),
                child: Text("Featured Photos",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: Container(
                  height: 100,
                  child: Row(
                    children: [
                      Image.asset(
                          'assets/images/pic1.png',
                          fit: BoxFit.fitWidth
                      ),

                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 0, 20),
                child: Text("All Photos",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: Container(
                  height: 150,
                  child: Row(
                    children: [
                      Image.asset(
                          'assets/images/pic41.png',
                          fit: BoxFit.fill
                      ),
                      Image.asset(
                          'assets/images/pic42.png',
                          fit: BoxFit.fill
                      ),
                    ],
                  ),
                ),
              ),


              SizedBox(
                height: 50,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(icon: Icon(Icons.home), onPressed: () {}),
              IconButton(icon: Icon(Icons.mark_email_unread), onPressed: () {}),
              IconButton(icon: Icon(Icons.add_alert), onPressed: () {}),
              IconButton(icon: Icon(Icons.person), onPressed: () {}),
            ],
          ),
        ),
      ),
      floatingActionButton:
      FloatingActionButton(
          backgroundColor: Colors.deepPurpleAccent,
          child: Icon(Icons.add,color: Colors.white,),
          onPressed: () {

          }),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  void onToggleShowPass() {
    setState(() {
      _showPass = !_showPass;
    });
  }

  void gotoSignInClicked() {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => LoginPage(),
        ));
  }

}