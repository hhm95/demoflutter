// @dart=2.9
import 'dart:convert';
List<DataChart> dataChartFromJson(String str) => List<DataChart>.from(json.decode(str).map((x) => DataChart.fromJson(x)));

String dataChartToJson(List<DataChart> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DataChart {
  DataChart({
    this.valueMesument,
    this.id,
  });

  List<ValueMesument> valueMesument;
  String id;

  factory DataChart.fromJson(Map<String, dynamic> json) => DataChart(
    valueMesument: List<ValueMesument>.from(json["valueMesument"].map((x) => ValueMesument.fromJson(x))),
    id: json["_id"],
  );

  Map<String, dynamic> toJson() => {
    "valueMesument": List<dynamic>.from(valueMesument.map((x) => x.toJson())),
    "_id": id,
  };
}

class ValueMesument {
  ValueMesument({
    this.eToday,
    this.pIn,
    this.pOut,
    this.pMaxtoday,
    this.vGrid,
    this.iGrid,
    this.fGrid,
    this.powerFactor,
    this.vPv1,
    this.iPv1,
    this.vPv2,
    this.iPv2,
    this.iLeakage,
    this.temp,
    this.sllk,
    this.ttghd,
    this.time,
    this.errcode,
  });

  String eToday;
  String pIn;
  String pOut;
  String pMaxtoday;
  String vGrid;
  String iGrid;
  String fGrid;
  String powerFactor;
  String vPv1;
  String iPv1;
  String vPv2;
  String iPv2;
  String iLeakage;
  String temp;
  String sllk;
  String ttghd;
  String time;
  String errcode;

  factory ValueMesument.fromJson(Map<String, dynamic> json) => ValueMesument(
    eToday: json["E_today"],
    pIn: json["P_in"],
    pOut: json["P_out"],
    pMaxtoday: json["P_maxtoday"],
    vGrid: json["V_grid"],
    iGrid: json["I_grid"],
    fGrid: json["F_grid"],
    powerFactor: json["power_factor"],
    vPv1: json["V_PV1"],
    iPv1: json["I_PV1"],
    vPv2: json["V_PV2"],
    iPv2: json["I_PV2"],
    iLeakage: json["I_leakage"],
    temp: json["Temp"],
    sllk: json["SLLK"],
    ttghd: json["TTGHD"],
    time: json["time"],
    errcode: json["Errcode"],
  );

  Map<String, dynamic> toJson() => {
    "E_today": eToday,
    "P_in": pIn,
    "P_out": pOut,
    "P_maxtoday": pMaxtoday,
    "V_grid": vGrid,
    "I_grid": iGrid,
    "F_grid": fGrid,
    "power_factor": powerFactor,
    "V_PV1": vPv1,
    "I_PV1": iPv1,
    "V_PV2": vPv2,
    "I_PV2": iPv2,
    "I_leakage": iLeakage,
    "Temp": temp,
    "SLLK": sllk,
    "TTGHD": ttghd,
    "time": time,
    "Errcode": errcode,
  };
}
