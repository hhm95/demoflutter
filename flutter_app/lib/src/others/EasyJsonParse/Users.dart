// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);
// @dart=2.9
import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    this.devices,
    this.historys,
    this.id,
    this.email,
    this.password,
    this.phonenumber,
    this.isadmin,
    this.firmwareCrc16,
    this.v,
    this.firmWare,
  });

  List<Devices> devices;
  List<String> historys;
  String id;
  String email;
  String password;
  int phonenumber;
  bool isadmin;
  String firmwareCrc16;
  int v;
  String firmWare;

  factory User.fromJson(Map<String, dynamic> json) => User(
    devices: List<Devices>.from(json["devices"].map((x) => Devices.fromJson(x))),
    historys: List<String>.from(json["historys"].map((x) => x)),
    id: json["_id"],
    email: json["email"],
    password: json["password"],
    phonenumber: json["phonenumber"],
    isadmin: json["isadmin"],
    firmwareCrc16: json["firmware_crc16"],
    v: json["__v"],
    firmWare: json["firm_ware"],
  );

  Map<String, dynamic> toJson() => {
    "devices": List<dynamic>.from(devices.map((x) => x.toJson())),
    "historys": List<dynamic>.from(historys.map((x) => x)),
    "_id": id,
    "email": email,
    "password": password,
    "phonenumber": phonenumber,
    "isadmin": isadmin,
    "firmware_crc16": firmwareCrc16,
    "__v": v,
    "firm_ware": firmWare,
  };
}

class Devices {
  Devices({
    this.imei,
    this.deviceName,
  });

  String imei;
  String deviceName;

  factory Devices.fromJson(Map<String, dynamic> json) => Devices(
    imei: json["Imei"],
    deviceName: json["Device_name"],
  );

  Map<String, dynamic> toJson() => {
    "Imei": imei,
    "Device_name": deviceName,
  };
}
