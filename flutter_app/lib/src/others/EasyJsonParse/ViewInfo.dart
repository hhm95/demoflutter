// To parse this JSON data, do
//
//     final viewInfo = viewInfoFromJson(jsonString);
// @dart=2.9
import 'dart:convert';

List<ViewInfo> viewInfoFromJson(String str) => List<ViewInfo>.from(json.decode(str).map((x) => ViewInfo.fromJson(x)));

String viewInfoToJson(List<ViewInfo> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ViewInfo {
  ViewInfo({
    this.id,
    this.imei,
    this.deviceKey,
    this.softwareKey,
    this.direction,
    this.name,
    this.vOut,
    this.vOutMax,
    this.valuePin,
    this.performance,
    this.sfVsDevice,
  });

  String id;
  String imei;
  String deviceKey;
  String softwareKey;
  String direction;
  String name;
  String vOut;
  String vOutMax;
  String valuePin;
  String performance;
  String sfVsDevice;

  factory ViewInfo.fromJson(Map<String, dynamic> json) => ViewInfo(
    id: json["_id"],
    imei: json["Imei"],
    deviceKey: json["Device_key"],
    softwareKey: json["Software_key"],
    direction: json["Direction"],
    name: json["Name"],
    vOut: json["V_out"],
    vOutMax: json["V_out_max"],
    valuePin: json["Value_Pin"],
    performance: json["Performance"],
    sfVsDevice: json["SfVs_device"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "Imei": imei,
    "Device_key": deviceKey,
    "Software_key": softwareKey,
    "Direction": direction,
    "Name": name,
    "V_out": vOut,
    "V_out_max": vOutMax,
    "Value_Pin": valuePin,
    "Performance": performance,
    "SfVs_device": sfVsDevice,
  };
}
