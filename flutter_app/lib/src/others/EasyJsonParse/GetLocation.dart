// To parse this JSON data, do
//
//     final getLocation = getLocationFromJson(jsonString);

import 'dart:convert';

GetLocation getLocationFromJson(String str) => GetLocation.fromJson(json.decode(str));

String getLocationToJson(GetLocation data) => json.encode(data.toJson());

class GetLocation {
  GetLocation({
    this.status,
    this.balance,
    this.lat,
    this.lon,
    this.accuracy,
    this.address,
  });

  String status;
  int balance;
  double lat;
  double lon;
  int accuracy;
  String address;

  factory GetLocation.fromJson(Map<String, dynamic> json) => GetLocation(
    status: json["status"],
    balance: json["balance"],
    lat: json["lat"].toDouble(),
    lon: json["lon"].toDouble(),
    accuracy: json["accuracy"],
    address: json["address"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "balance": balance,
    "lat": lat,
    "lon": lon,
    "accuracy": accuracy,
    "address": address,
  };
}
