// To parse this JSON data, do
//
//     final getSmonth = getSmonthFromJson(jsonString);
// @dart=2.9
import 'dart:convert';

GetSmonth getSmonthFromJson(String str) => GetSmonth.fromJson(json.decode(str));

String getSmonthToJson(GetSmonth data) => json.encode(data.toJson());

class GetSmonth {
  GetSmonth({
    this.imei,
    this.month,
    this.sumEtoday,
  });

  String imei;
  String month;
  String sumEtoday;

  factory GetSmonth.fromJson(Map<String, dynamic> json) => GetSmonth(
    imei: json["imei"],
    month: json["month"],
    sumEtoday: json["sum_etoday"],
  );

  Map<String, dynamic> toJson() => {
    "imei": imei,
    "month": month,
    "sum_etoday": sumEtoday,
  };
}
