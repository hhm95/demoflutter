// To parse this JSON data, do
//
//     final userViewDevice = userViewDeviceFromJson(jsonString);
// @dart=2.9
import 'dart:convert';

UserViewDevice userViewDeviceFromJson(String str) => UserViewDevice.fromJson(json.decode(str));

String userViewDeviceToJson(UserViewDevice data) => json.encode(data.toJson());

class UserViewDevice {
  UserViewDevice({
    this.username,
    this.devices,
    this.isadmins,
    this.historys,
    this.lists,
    this.listerrs,
  });

  String username;
  List<Device> devices;
  bool isadmins;
  List<String> historys;
  List<ListElement> lists;
  List<Listerr> listerrs;

  factory UserViewDevice.fromJson(Map<String, dynamic> json) => UserViewDevice(
    username: json["username"],
    devices: List<Device>.from(json["devices"].map((x) => Device.fromJson(x))),
    isadmins: json["isadmins"],
    historys: List<String>.from(json["historys"].map((x) => x)),
    lists: List<ListElement>.from(json["lists"].map((x) => ListElement.fromJson(x))),
    listerrs: List<Listerr>.from(json["listerrs"].map((x) => Listerr.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "username": username,
    "devices": List<dynamic>.from(devices.map((x) => x.toJson())),
    "isadmins": isadmins,
    "historys": List<dynamic>.from(historys.map((x) => x)),
    "lists": List<dynamic>.from(lists.map((x) => x.toJson())),
    "listerrs": List<dynamic>.from(listerrs.map((x) => x.toJson())),
  };
}

class Device {
  Device({
    this.imei,
    this.deviceName,
  });

  String imei;
  String deviceName;

  factory Device.fromJson(Map<String, dynamic> json) => Device(
    imei: json["Imei"],
    deviceName: json["Device_name"],
  );

  Map<String, dynamic> toJson() => {
    "Imei": imei,
    "Device_name": deviceName,
  };
}

class Listerr {
  Listerr({
    this.imei,
    this.errcode,
    this.time,
    this.time2,
  });

  String imei;
  String errcode;
  String time;
  String time2;

  factory Listerr.fromJson(Map<String, dynamic> json) => Listerr(
    imei: json["Imei"],
    errcode: json["Errcode"],
    time: json["time"],
  );

  Map<String, dynamic> toJson() => {
    "Imei": imei,
    "Errcode": errcode,
    "time": time,
  };
}

class ListElement {
  ListElement({
    this.imei,
    this.ttg,
    this.sllk,
    this.etoday,
    this.name
  });

  String imei;
  String ttg;
  String sllk;
  String etoday;
  String name;

  factory ListElement.fromJson(Map<String, dynamic> json) => ListElement(
    imei: json["Imei"],
    ttg: json["TTG"],
    sllk: json["SLLK"],
    etoday: json["E_today"],
  );

  Map<String, dynamic> toJson() => {
    "Imei": imei,
    "TTG": ttg,
    "SLLK": sllk,
    "E_today" : etoday,
  };
}
