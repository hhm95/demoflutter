import 'package:flutter/material.dart';
import 'package:flutter_app/src/common/app_colors.dart';

class AvatarCircle extends StatelessWidget {

  final String imAvatar;
  final double sizeHeight;

  AvatarCircle(this.imAvatar, this.sizeHeight);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: sizeHeight,
      height: sizeHeight,
      // padding: EdgeInsets.all(4),
      // decoration: BoxDecoration(
      //   borderRadius: BorderRadius.circular(sizeHeight/2),
      //   border: Border.all(color: AppColors.blue, width: 2)
      // ),
      child: Image.asset(imAvatar),

    );
  }
}
