import 'package:flutter/cupertino.dart';
import 'package:flutter_app/src/common/app_colors.dart';

class AppTextStyle {

  static const TextStyle t14B = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w700
  );

  static const TextStyle t14N = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.normal
  );

  static const TextStyle t16B = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold
  );

  static const TextStyle t16N = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.normal
  );

  static const TextStyle t26B = TextStyle(
      fontSize: 26,
      fontWeight: FontWeight.bold
  );

  static const TextStyle t12T = TextStyle(
      fontSize: 12,
      color: AppColors.grey,
  );

  static const TextStyle t12N = TextStyle(
      fontSize: 12,
      fontWeight: FontWeight.normal
  );

}