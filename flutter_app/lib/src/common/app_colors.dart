import 'package:flutter/cupertino.dart';

class AppColors {

  static const Color RED = Color(0xFFFF0000);
  static const Color GREEN = Color(0xFF00FF00);
  static const Color BLUE = Color(0xFF0000FF);

  static const Color dark = Color(0xFF223263);
  static const Color blue = Color(0xFF40BFFF);
  static const Color red = Color(0xFFFB7181);
  static const Color grey = Color(0xFF8B7C7C);

}