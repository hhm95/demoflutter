class AppImages {
  //--- image ---
  static const String imLog = 'assets/images/logo.png';
  static const String imUserInit = 'assets/images/initpic.png';
  static const String imUser1 = 'assets/images/pic31.png';
  static const String imUser2 = 'assets/images/pic32.png';
  static const String imUser3 = 'assets/images/pic33.png';
  static const String imUser4 = 'assets/images/pic34.png';


  // ---- icons -----
  static const String icHeart = 'assets/icons/icHeart.png';

}