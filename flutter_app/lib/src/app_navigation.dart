import 'package:flutter/material.dart';
import 'package:flutter_app/src/app/home/home_screen.dart';
import 'package:flutter_app/src/app/notification/notification_screen.dart';
import 'package:flutter_app/src/app/user/user_profile.dart';
import 'package:flutter_app/src/common/app_colors.dart';

class AppNavigation extends StatefulWidget {
  @override
  _AppNavigationState createState() => _AppNavigationState();
}

class _AppNavigationState extends State<AppNavigation> {

  var _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _currentIndex,
        children: [
          HomeScreen(),
          Container(color: AppColors.BLUE,),
          NotificationScreen(),
          UserProfileScreen()
        ],
      ),
      bottomNavigationBar: _buiBottomNavigation()
    );
  }

  _buiBottomNavigation() {
    return BottomNavigationBar(

      type: BottomNavigationBarType.fixed,
      selectedItemColor: AppColors.blue,
      unselectedItemColor: AppColors.grey,
      currentIndex: _currentIndex,
      onTap: (index) {
        setState(() {
          _currentIndex = index;
        });
      },
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
        BottomNavigationBarItem(icon: Icon(Icons.mark_email_unread), label: 'Mail'),
        BottomNavigationBarItem(icon: Icon(Icons.add_alert), label: 'Notification'),
        BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
      ]
    );
  }
}
