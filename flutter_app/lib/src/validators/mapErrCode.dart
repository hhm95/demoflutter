
  //err code list

  List<String> mean = [
    '00',//00
    'Lỗi rơle',//01
    'Lỗi TripZone Event',//02
    'Lỗi mạch boost',//03
    'Không có đầu vào PV',//04
    'Lỗi điện áp đầu lưới đầu ra',//05
    'Loi 06',
    'Lỗi quá nhiệt độ trên vỏ',
    'Lỗi quá nhiệt độ bên trong',
    'Lỗi nhiệt độ thấp',
    'Loi 10',
    'Loi 11',
    'Loi 12',
    'Loi 13',
    'Loi 14',
    'Lỗi quạt.',//15
    'Loi 16',
    'Loi 17',
    'Loi 18',
    'Loi 19',
    'Loi 20',
    'Quá dòng đầu vào PV1.',//21
    'Quá dòng đầu vào PV2.',//22
    'Điện áp đầu vào PV1 quá ngưỡng trên.',//23
    'Điện áp đầu vào PV2 quá ngưỡng trên.',//24
    'Điện áp đầu vào PV1 quá ngưỡng dưới.',//25
    'Điện áp đầu vào PV2 quá ngưỡng dưới.',//26
    'Quá dòng AC.',//27
    'Điện áp lưới quá ngưỡng trên khi khởi động.',
    'Điện áp lưới quá ngưỡng trên khi đang hoạt động.',
    'Điện áp lưới trong khoảng cảnh báo ngưỡng trên quá thời gian cho phép khi đang hoạt động.',
    'Điện áp lưới quá ngưỡng dưới khi khởi động.',//31
    'Điện áp lưới quá ngưỡng dưới khi đang hoạt động.',
    'Điện áp lưới trong khoảng cảnh báo ngưỡng dưới quá thời gian cho phép khi đang hoạt động.',
    'Quá dòng rò (RCD).',
    'Quá dòng DC vào lưới.',
    'Tần số lưới quá ngưỡng trên khi khởi động.',
    'Tần số lưới quá ngưỡng trên khi đang hoạt động.',
    'Tần số lưới trong khoảng cảnh báo ngưỡng trên quá thời gian cho phép khi đang hoạt động.',
    'Tần số lưới quá ngưỡng dưới khi khởi động.',
    'Tần số lưới quá ngưỡng dưới khi đang hoạt động.',
    'Tần số lưới trong khoảng cảnh báo ngưỡng dưới quá thời gian cho phép khi đang hoạt động.',//41
    'Lỗi chạm đất PV.',//42
    'Lỗi chạm đất AC. Chạm LINE.',//43
    'Lỗi chạm đất AC. Chạm NEUTRAL.',//44
    'Quá công suất đầu ra.',//46
    'Quá công suất đầu vào.',//45
    'Điện áp DC bus quá cao.',//47
    'Lỗi cảm biến đo điện áp boost.',//48
    'Lỗi cảm biến đo dòng rò.',//49
    'Lỗi cảm biến đo điện áp PV.',//50
    'Điện áp PV1 thay đổi đột ngột.',//51
    'Điện áp PV2 thay đổi đột ngột.',//52
    'Lỗi quá dòng điều khiển AC.',//53
    'Lỗi quá dòng điều khiển DC',//54
    'Lỗi quá áp điều khiển',//55
    'Islanding.',//56
  ];
List<String> fix = [
  'Fix 00',
  '1. Chờ thiết bị khởi động lại.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Chờ thiết bị khởi động lại.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Chờ thiết bị khởi động lại.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Chờ thiết bị khởi động lại.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Chờ thiết bị khởi động lại.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  'Huong dan fix Loi 06',
  '1. Chờ thiết bị khởi động lại.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Chờ thiết bị khởi động lại.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Chờ thiết bị khởi động lại.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  'Huong dan fix Loi 10',
  'Huong dan fix Loi 11',
  'Huong dan fix Loi 12',
  'Huong dan fix Loi 13',
  'Huong dan fix Loi 14',
  'Tắt thiết bị, ngắt kết nối AC và PV.\nChờ 30 phút để thiết bị xả tụ.\nMở thiết bị kiểm tra quạt xem có bị kẹt, hỏng không. Nếu hỏng, thay quạt mới.',
  'Huong dan fix Loi 16',
  'Huong dan fix Loi 17',
  'Huong dan fix Loi 18',
  'Huong dan fix Loi 19',
  'Huong dan fix Loi 20',
  'Kiểm tra lại cách đấu nối chuỗi pin PV1',
  'Kiểm tra lại cách đấu nối chuỗi pin PV2',
  'Kiểm tra lại cách đấu nối chuỗi pin PV1',
  'Kiểm tra lại cách đấu nối chuỗi pin PV2',
  'Kiểm tra lại cách đấu nối chuỗi pin PV1',
  'Kiểm tra lại cách đấu nối chuỗi pin PV2',//26
  '1. Thiết bị sẽ tiếp tục hoạt động lại khi dòng điện ra dưới ngưỡng bảo vệ.\n2. Nếu tiếp tục lỗi, liên hệ nhà sản xuất.',
  '1. Kiểm tra điện áp lưới.\n2. Nếu điện áp lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu điện áp lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra điện áp lưới.\n2. Nếu điện áp lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu điện áp lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra điện áp lưới.\n2. Nếu điện áp lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu điện áp lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra điện áp lưới.\n2. Nếu điện áp lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu điện áp lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra điện áp lưới.\n2. Nếu điện áp lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu điện áp lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra điện áp lưới.\n2. Nếu điện áp lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu điện áp lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra lỗi PV chạm đất.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',//34
  '1. Chờ thiết bị khởi động lại.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Kiểm tra tần số lưới.\n2. Nếu tần số lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu tần số lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra tần số lưới.\n2. Nếu tần số lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu tần số lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra tần số lưới.\n2. Nếu tần số lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu tần số lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra tần số lưới.\n2. Nếu tần số lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu tần số lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra tần số lưới.\n2. Nếu tần số lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu tần số lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra tần số lưới.\n2. Nếu tần số lưới vượt quá ngưỡng bảo vệ của thiết bị, liên hệ đơn vị cung cấp điện để hỏi nguyên nhân và giải pháp.\n3. Nếu tần số lưới nằm trong dải hoạt động của thiết bị, liên hệ nhà sản xuất.',
  '1. Kiểm tra lại các cáp PV có chạm đất không.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',//42
  '1. Kiểm tra lại cáp AC (L, N) có chạm đất không.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Kiểm tra lại cáp AC (L, N) có chạm đất không.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  'Kiểm tra lại cách đấu nối chuỗi pin PV',
  '1. Khởi động lại thiết bị.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Chờ thiết bị khởi động lại.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',//47
  '1. Khởi động lại thiết bị.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Khởi động lại thiết bị.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Khởi động lại thiết bị.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',
  '1. Khởi động lại thiết bị.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',//51
  '1. Khởi động lại thiết bị.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',//52
  '1. Khởi động lại thiết bị.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',//53
  '1. Khởi động lại thiết bị.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',//54
  '1. Khởi động lại thiết bị.\n2. Nếu lỗi tiếp tục lặp lại, liên hệ nhà sản xuất.',//55
  '1. Kiểm tra Aptomat AC có bị ngắt không.\n2. Kiểm tra kỹ cáp AC xem có bị lỏng, không kết nối được.\n3. Kiểm tra lưới điện hiện tại có bị mất hay không.\n4. Nếu toàn bộ kiểm tra trên OK và lỗi này tiếp tục xảy ra, liên hệ nhà sản xuất.',//56
];