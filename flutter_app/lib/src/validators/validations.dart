class Validations {
  static bool isValidUser(String user){
    return user.length >3;
  }

  static bool isValidPass(String pass){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(pass);
    //return pass.length >6;
  }

  static bool isValidPhone(String phone){
    return phone.length == 10;
  }

  static bool isValidLink(String link){
    return link.length >10;
  }
}